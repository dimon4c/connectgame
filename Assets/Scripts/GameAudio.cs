﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class Game
{
    public List<GameObject> muteBtns;
    public List<GameObject> unmuteBtns;
    public List<AudioClip> audioClips;
    AudioSource aSource;

    public void MuteSound()
    {
        foreach (GameObject go in unmuteBtns)
        {
            go.SetActive(false);

        }
        foreach (GameObject go in muteBtns)
        {
            go.SetActive(true);
        }
        AudioListener.pause = true;
    }

    public void UnmuteSound()
    {
        foreach (GameObject go in unmuteBtns)
        {
            go.SetActive(true);

        }
        foreach (GameObject go in muteBtns)
        {
            go.SetActive(false);
           
        }
        AudioListener.pause = false;
    }

    void CheckAudio()
    {
        bool muted = AudioListener.pause;
        foreach (GameObject go in unmuteBtns)
        {
            go.SetActive(!muted);

        }
        foreach (GameObject go in muteBtns)
        {
            go.SetActive(muted);

        }
    }

    public void PlaySound(int audioIndex)
    {
        //added

        if (audioIndex < audioClips.Count)
            aSource.Stop();
        aSource.clip = audioClips[audioIndex];
        aSource.Play();
    }
}
