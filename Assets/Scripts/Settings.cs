﻿using System;

public class Settings
{
    //-------------------------------------------------
    public static Point FieldSizeMax = new Point(10, 10);
    public static Point FieldSizeClassic = new Point(5, 8);
    public static Point FieldSizeExtreme = new Point(7, 8);
    public static int cellsToSetClassic = 5;
    public static int cellsToSetExtreme = 6;
    //-------------------------------------------------
}

