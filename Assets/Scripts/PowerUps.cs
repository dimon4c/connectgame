﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum PowerUps
{
    None,
    Hammer,
    Swap,
    RemoveValue,
    Clear}
;


public partial class Game : MonoBehaviour
{
    public GameObject hammerHelp, swapHelp, removeHelp;
    public PowerUps enabledNow = PowerUps.None;
    List<Cell> toSwap = new List<Cell>();
    int[] powerCostDefault = { 0, 100, 100, 100, 100 };
    int[] powerCost = { 0, 100, 100, 100, 100 };
    public Text hammerCost, swapCost, removeValueCost;
    public Sprite[] LSpriteHammer;
    public Sprite[] RSpriteHammer;
    public GameObject hammer, swap, clear, removeValue;
    public GameObject hammerAnim;
    SpriteRenderer LHammer, RHammer;
    public List<PowerUps> activated = new List<PowerUps>();
    bool swapHint = false;
    bool remHint = false;
    public Sprite disabledSwap, enabledSwap, enabledHammer, disabledHammer, enabledRemover, disabledRemover;

    void EnablePowerAnim()
    {
        if (powerCost[(int)PowerUps.Hammer] <= PlayerMoney)
            AnimatePowerup(PowerUps.Hammer, true);
        if (powerCost[(int)PowerUps.Swap] <= PlayerMoney)
            AnimatePowerup(PowerUps.Swap, true);
        if (powerCost[(int)PowerUps.RemoveValue] <= PlayerMoney)
            AnimatePowerup(PowerUps.RemoveValue, true);
    }

    void DisablePowerAnim()
    {
        hammer.SetActive(false);
        hammer.transform.localScale = new Vector3(1f, 1f, 1f);
        hammer.SetActive(true);

        swap.SetActive(false);
        swap.transform.localScale = new Vector3(1f, 1f, 1f);
        swap.SetActive(true);

        removeValue.SetActive(false);
        removeValue.transform.localScale = new Vector3(1f, 1f, 1f);
        removeValue.SetActive(true);
        /*/
        AnimatePowerup(PowerUps.Hammer, false);
        AnimatePowerup(PowerUps.Swap, false);
        AnimatePowerup(PowerUps.RemoveValue, false);
        //*/
    }

    void AnimatePowerup(PowerUps powerup, bool enable)
    {
        switch (powerup)
        {
            case PowerUps.Hammer:
                if (enable)
                    hammer.GetComponent<Animator>().Play("Hint");
                else
                    hammer.GetComponent<Animator>().Play("Idle");
                break;
            case PowerUps.Swap:
                if (enable)
                    swap.GetComponent<Animator>().Play("Hint");
                else
                    swap.GetComponent<Animator>().Play("Idle");
                break;
            case PowerUps.RemoveValue:
                if (enable)
                    removeValue.GetComponent<Animator>().Play("Hint");
                else
                    removeValue.GetComponent<Animator>().Play("Idle");

                break;
        }
    }

    public void ActivatePowerUp()
    {
        for (int i = 0; i < powerCost.Length; i++)
        {
            powerCost[i] = powerCostDefault[i];
        }
        UpdatePrices(PowerUps.Hammer);
        UpdatePrices(PowerUps.Swap);
        UpdatePrices(PowerUps.RemoveValue);


        swapHint = false;
        remHint = false;
    }

    public void ShowHammerAnim(Vector3 position, Color color = default (Color))
    {
        LHammer = hammerAnim.transform.GetChild(0).gameObject.GetComponent<SpriteRenderer>();
        RHammer = hammerAnim.transform.GetChild(1).gameObject.GetComponent<SpriteRenderer>();
        LHammer.sprite = LSpriteHammer[CurrentSkin];
        RHammer.sprite = RSpriteHammer[CurrentSkin];
        if (color == default(Color))
        {
            color = Color.white;
        }
        LHammer.color = color;
        RHammer.color = color;


        hammerAnim.transform.position = position;
        hammerAnim.SetActive(false);
        hammerAnim.SetActive(true);
    }

    void CheckDisable()
    {
        if (PlayerPrefs.GetInt("SwapHelp", 0) == 1)
        {
            swap.transform.GetChild(1).gameObject.SetActive(false);
        }
        if (PlayerPrefs.GetInt("HammerHelp", 0) == 1)
        {
            hammer.transform.GetChild(0).gameObject.SetActive(false);
        }
        if (PlayerPrefs.GetInt("RemoveHelp", 0) == 1)
        {
            removeValue.transform.GetChild(1).gameObject.SetActive(false);
        }
    }

    void UpdatePrices(PowerUps powerType)
    {
        switch (powerType)
        {
            case PowerUps.Hammer:
                hammerCost.text = powerCost[(int)powerType].ToString();
                break;
            case PowerUps.Swap:
                swapCost.text = powerCost[(int)powerType].ToString();
                break;
            case PowerUps.RemoveValue:
                removeValueCost.text = powerCost[(int)powerType].ToString();
                break;
            default:
                //Debug.Log("Unusual power type");
                break;
        }
    }

    void Revive()
    {
        revived = true;
        endPointMode = false;
        if (PlayMode == PlayMode.Classic)
            RemoveCells(13);
        else
            RemoveCells(20);
        UIGameOverPanel.SetActive(false);
        SetAllCellsInteraction(true);
        UpdateScore();
    }

    void ShowActivated()
    {
        if (activated == null)
            activated = new List<PowerUps>();
       
        foreach (PowerUps id in activated)
        {
            //Debug.Log(id);

            switch (id)
            {
                case PowerUps.Clear:
                    clear.SetActive(true);
                    break;
                case PowerUps.Hammer:
                    hammer.SetActive(true);
                    break;
                case PowerUps.RemoveValue:
                    removeValue.SetActive(true);
                    break;
                case PowerUps.Swap:
                    swap.SetActive(true);
                    break;
               
                default:
                    //Debug.Log("Unusual behaviour incorrect powerUp type");
                    break;
            }


         
        }
    }

    void ReturnPowerUp(int type)
    {
        if (activated.Contains((PowerUps)type))
        {
            activated.Remove((PowerUps)type);
            PlayerMoney += powerCost[type];
        }
        
    }

    #region RemoveCells

    public void RemoveCells(int amount)
    {
        IncorrectPathPopUp(false);
        List<Cell> usedCells = new List<Cell>();
        for (int i = 0; i < fieldSize.X; i++)
        {
            for (int j = 0; j < fieldSize.Y; j++)
            {
                if (Field[i, j].used)
                    usedCells.Add(Field[i, j]);
            }
        }
        if (usedCells.Count <= amount)
            amount = usedCells.Count - 1;

        for (int i = 0; i < amount; i++)
        {
            int index = Random.Range(0, usedCells.Count);
            usedCells[index].anim.Play("Revive");
//            usedCells[index].ResetValue();

            usedCells.RemoveAt(index);
        }
    }


    #endregion

    #region Hammer

    public void ShowHammerHelp()
    {
        SetAllCellsInteraction(false);
        PlayerPrefs.SetInt("HammerHelp", 1);
        CheckDisable();
        hammerHelp.SetActive(true);
    }

    public void HideHammerHelp()
    {
        hammerHelp.SetActive(false);
        SetAllCellsInteraction(true);
    }

    public void EnableHammer()
    {
        if (powerCost[(int)PowerUps.Hammer] > PlayerMoney && PlayerPrefs.GetInt("HammerHelp", 0) == 1)
        {
            UIOpenShop();
            return;
        }

        if (PlayerPrefs.GetInt("HammerHelp", 0) == 0)
        {
            ShowHammerHelp();
            return;
        }

        IncorrectPathPopUp(false);
       
        if (enabledNow == PowerUps.None && powerCost[(int)PowerUps.Hammer] <= PlayerMoney)
        {
            hammer.GetComponent<Image>().sprite = enabledHammer;
            enabledNow = PowerUps.Hammer;
            for (int i = 0; i < fieldSize.X; i++)
            {
                for (int j = 0; j < fieldSize.Y; j++)
                {
                    if (Field[i, j].used)
                        Field[i, j].EnableHammer();
                }
            }
        }
        else if (enabledNow == PowerUps.Hammer)
        {
            DisableHammer();

        }

    }

    public void DisableHammer(bool reduceMoney = false)
    {
        if (reduceMoney)
        {
            PlayerMoney -= powerCost[(int)PowerUps.Hammer];
            UpdateScore();
            powerCost[(int)PowerUps.Hammer] *= 2;
            UpdatePrices(PowerUps.Hammer);
        }
        enabledNow = PowerUps.None;
        for (int i = 0; i < fieldSize.X; i++)
        {
            for (int j = 0; j < fieldSize.Y; j++)
            {
                if (Field[i, j].used)
                    Field[i, j].DisableHammer();
            }
        }
        hammer.GetComponent<Image>().sprite = disabledHammer;
        if (GetUsedCells().Count - cellToIgnore < 1)
        {
            GeneratePresets(cellsToSet);
            StartCoroutine(SetRandomCellsVal());
        }
    }

    #endregion

    void DisableOneGameHint()
    {
        removeValue.transform.GetChild(0).gameObject.SetActive(false);
        swap.transform.GetChild(0).gameObject.SetActive(false);
    }

    #region Swap

    public void ShowSwapHelp()
    {
        SetAllCellsInteraction(false);
        PlayerPrefs.SetInt("SwapHelp", 1);
        CheckDisable();
        swapHelp.SetActive(true);
    }

    public void HideSwapHelp()
    {
        swapHelp.SetActive(false);
        SetAllCellsInteraction(true);
    }

    public void EnableSwap()
    {
        if (powerCost[(int)PowerUps.Swap] > PlayerMoney && PlayerPrefs.GetInt("SwapHelp", 0) == 1)
        {
            UIOpenShop();
            return;
        }
        if (PlayerPrefs.GetInt("SwapHelp", 0) == 0)
        {
            ShowSwapHelp();
            return;
        }

        IncorrectPathPopUp(false);
        if (enabledNow == PowerUps.None && powerCost[(int)PowerUps.Swap] <= PlayerMoney)
        {
            for (int i = 0; i < fieldSize.X; i++)
            {
                for (int j = 0; j < fieldSize.Y; j++)
                {
                    if (Field[i, j].used)
                        Field[i, j].EnableSwapMode();
                }
            }
            enabledNow = PowerUps.Swap;
            swap.GetComponent<Image>().sprite = enabledSwap;
            if (!swapHint)
            {
                swapHint = true;
                swap.transform.GetChild(0).gameObject.SetActive(true);
                Invoke("DisableOneGameHint", 2f);
            }

        }
        else if (enabledNow == PowerUps.Swap)
        {
            DisableSwap();

        }
       
    }

    public void DisableSwap(bool reduceMoney = false)
    {
        if (reduceMoney)
        {
            PlayerMoney -= powerCost[(int)PowerUps.Swap];
            UpdateScore();
            powerCost[(int)PowerUps.Swap] *= 2;
            UpdatePrices(PowerUps.Swap);
        }

        enabledNow = PowerUps.None;
        for (int i = 0; i < fieldSize.X; i++)
        {
            for (int j = 0; j < fieldSize.Y; j++)
            {
                if (Field[i, j].used)
                    Field[i, j].DisableSwapMode();
            }
        }
        swap.GetComponent<Image>().sprite = disabledSwap;
       

        foreach (Cell c in toSwap)
        {
            c.UnsetPressed();
        }
    }

    public void SwapCells(Cell choosenCell)
    {
        toSwap.Add(choosenCell);
        if (toSwap.Count == 2)
        {
            if (!toSwap[0].Equals(toSwap[1]) && toSwap[1].used)
            {
                DoSwap();
            }
            else
            {
                toSwap[0].UnsetPressed();
                toSwap[1].UnsetPressed();
                toSwap = new List<Cell>();
                DisableSwap();
            }
            
        }

        
    }

    void DoSwap()
    {
//        enabledNow = PowerUps.None;
        int temp = toSwap[0].Value;
//        List<Point> path = new List<Point>();
//        path.Add(toSwap[1].cellPos);
//        toSwap[1].ResetValue();
        GameObject go1 = Field[toSwap[0].cellPos.X, toSwap[0].cellPos.Y].gameObject;
        GameObject go2 = Field[toSwap[1].cellPos.X, toSwap[1].cellPos.Y].gameObject;

        StartCoroutine(MoveCellSwap(toSwap[0].cellPos, toSwap[1].cellPos, go1, go2));
       
//        path = new List<Point>();
//        path.Add(toSwap[0].cellPos);
//        StartCoroutine(MoveCell(path, Field[toSwap[0].cellPos.X, toSwap[0].cellPos.Y].gameObject));
        /*
        toSwap[0].SetValue(toSwap[1].Value);
        toSwap[1].SetValue(temp);
        toSwap[0].gameObject.GetComponent<SpriteRenderer>().sprite = usedCellType[CurrentSkin];
        toSwap[0].colorId = SetCorrespondingColor(toSwap[0].gameObject.GetComponent<SpriteRenderer>(), toSwap[0].Value);
        toSwap[1].gameObject.GetComponent<SpriteRenderer>().sprite = usedCellType[CurrentSkin];
        toSwap[1].colorId = SetCorrespondingColor(toSwap[1].gameObject.GetComponent<SpriteRenderer>(), toSwap[1].Value);

            */

//        CheckConnectedCells(toSwap[0].cellPos);
//        CheckConnectedCells(toSwap[1].cellPos);
        toSwap = new List<Cell>();
    }

    #endregion

    #region RemoveByValue

    public void ShowRemoveHelp()
    {
        SetAllCellsInteraction(false);
        PlayerPrefs.SetInt("RemoveHelp", 1);
        CheckDisable();
        removeHelp.SetActive(true);
    }

    public void HideRemoveHelp()
    {
        removeHelp.SetActive(false);
        SetAllCellsInteraction(true);
    }

    public void EnableRemover()
    {
        if (powerCost[(int)PowerUps.RemoveValue] > PlayerMoney && PlayerPrefs.GetInt("RemoveHelp", 0) == 1)
        {
            UIOpenShop();
            return;
        }
        if (PlayerPrefs.GetInt("RemoveHelp", 0) == 0)
        {
            ShowRemoveHelp();
            return;
        }

        IncorrectPathPopUp(false);

        if (enabledNow == PowerUps.None && powerCost[(int)PowerUps.RemoveValue] <= PlayerMoney)
        {
            enabledNow = PowerUps.RemoveValue;
            removeValue.GetComponent<Image>().sprite = enabledRemover;
            if (!remHint)
            {
                remHint = true;
                removeValue.transform.GetChild(0).gameObject.SetActive(true);
                Invoke("DisableOneGameHint", 2f);
            }

        }
        else if (enabledNow == PowerUps.RemoveValue)
        {
            DisableRemover();
        }
       

    }

    public void DisableRemover(bool reduceMoney = false)
    {
        if (reduceMoney)
        {
            PlayerMoney -= powerCost[(int)PowerUps.RemoveValue];
            UpdateScore();
            powerCost[(int)PowerUps.RemoveValue] *= 2;
            UpdatePrices(PowerUps.RemoveValue);

        }
        enabledNow = PowerUps.None;
        removeValue.GetComponent<Image>().sprite = disabledRemover;
       


    }

    public void RemoveByValue(int cellValue)
    {
        DisableRemover(true);

        for (int i = 0; i < fieldSize.X; i++)
        {
            for (int j = 0; j < fieldSize.Y; j++)
            {
                if (Field[i, j].Value == cellValue)
                {
                    Field[i, j].ResetValue();
                }
            }
        }
        if (GetUsedCells().Count - cellToIgnore < 1)
        {
            GeneratePresets(cellsToSet);
            StartCoroutine(SetRandomCellsVal());
        }
    }

    #endregion
}
