﻿using UnityEngine;
using System.Collections;
using GooglePlayGames;
using UnityEngine.SocialPlatforms;

public partial class Game
{

    //----------------------------------------------
    static bool LoggedIn = false;
    static bool AttempedToLoginToday = false;
    #if UNITY_ANDROID
    const string leaderboard1 = "CgkI0pqv1MINEAIQBg";
    const string leaderboard2 = "CgkI0pqv1MINEAIQBw";
    #elif UNITY_IPHONE
	const string leaderboard1 = "com.numbers.connect.consumable.l1";
	const string leaderboard2 = "com.numbers.connect.consumable.l2";
	
#elif UNITY_EDITOR
	const string leaderboard = "xxx";
	#endif

    string leaderboard;

    //----------------------------------------------
    public void  InitGameServices()
    {
        #if UNITY_ANDROID
        PlayGamesPlatform.Activate();
        PlayGamesPlatform.DebugLogEnabled = false;
        leaderboard = leaderboard1;
        #endif
        FirstLogin();
    }
    //----------------------------------------------
    void Authenticate()
    {			
        // authenticate user:
        Social.localUser.Authenticate((bool success) =>
            {
                // handle success or failure
                LoggedIn = success;
                if (!success)
                {
                    PlayerPrefs.SetInt("declined_play_services", 1);
                }
                AttempedToLoginToday = true;
            });
    }
    //----------------------------------------------
    public void FirstLogin()
    {
        int earlierDeclined = PlayerPrefs.GetInt("declined_play_services", 0);
        if (earlierDeclined == 1)
            return;
        if (AttempedToLoginToday && !LoggedIn)
            return;
        Authenticate();

    }
    //----------------------------------------------
    public void RequestedLogin()
    {		
        Authenticate();
    }
    //----------------------------------------------
    public void ReportScore(int score, int id)
    {		
        if (!LoggedIn)
            return;

        if (id == 1)
        {
            leaderboard = leaderboard1;
        }
        else
        {
            leaderboard = leaderboard2;
        }
        Social.ReportScore(score, leaderboard, (bool success) =>
            {
                // handle success or failure
            });

    }
    //----------------------------------------------
    public void ReportAchievments(string uniqueCode)
    {
        ReportAchievement(uniqueCode);
    }

    public void Report100Points()
    {
        ReportAchievement("ach_100_points");
    }
    //----------------------------------------------
    public void Report1000Points()
    {
        ReportAchievement("ach_1000_points");
    }
    //----------------------------------------------
    public void Report10000Points()
    {
        ReportAchievement("ach_10000_points");
    }
    //----------------------------------------------
    public void ReportAchievement(string name)
    {
        if (!LoggedIn)
            return;
        Social.ReportProgress(name, 100.0f, (bool success) =>
            {
                // handle success or failure
            });
    }
    //----------------------------------------------
    public void ShowLeaderboard()
    {        
        #if UNITY_ANDROID

        if (LoggedIn)
        {
            ((PlayGamesPlatform)Social.Active).ShowLeaderboardUI();
        }
        else
        {
            // authenticate user:
            Social.localUser.Authenticate((bool success) =>
                {
                    LoggedIn = success;
                    if (success)
                    {
                        ((PlayGamesPlatform)Social.Active).ShowLeaderboardUI();
                        PlayerPrefs.SetInt("declined_play_services", 0);
                    }
                });
        } 
        #endif

        #if UNITY_IPHONE
	if (LoggedIn) {
	(Social.Active).ShowLeaderboardUI();
	} else {
	// authenticate user:
	Social.localUser.Authenticate ((bool success) => {
	LoggedIn = success;
	if (success) {
	(Social.Active).ShowLeaderboardUI ();
	PlayerPrefs.SetInt("declined_play_services", 0);
	}
	});
	} 
        #endif
    }

}
