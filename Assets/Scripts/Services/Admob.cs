﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleMobileAds.Api;
using System;

public partial class Game
{

    //    public static Admob Instance;

    #if UNITY_IOS
    
	string BannerId = "ca-app-pub-9214247153547496/5964200760";
	string InterstitialId = "ca-app-pub-9214247153547496/7440933960";
	string StaticInterstitialId = "ca-app-pub-9214247153547496/8917667167";
	string VideoId = "ca-app-pub-9214247153547496/1394400368";

	
#else

    string BannerId = "ca-app-pub-9214247153547496/5258340361";
    string InterstitialId = "ca-app-pub-9214247153547496/6735073561";
    string StaticInterstitialId = "ca-app-pub-9214247153547496/9688539964";
    string VideoId = "ca-app-pub-9214247153547496/2165273165";

    #endif


    BannerView bannerView;
    InterstitialAd interstitial;
    InterstitialAd staticInterstitial;
    RewardBasedVideoAd rewardBasedVideo;




    void InitAdmob()
    {

        LoadBanner();
        LoadInterstitial();
        LoadStaticInterstitial();

        rewardBasedVideo = RewardBasedVideoAd.Instance;
        rewardBasedVideo.OnAdRewarded += HandleRewardBasedVideoRewarded;
        rewardBasedVideo.OnAdLoaded += HandleRewardBasedVideoLoaded;

        LoadVideo();

    }

    //---------------------------------------------------------------------------
    void LoadBanner()
    {
        if (HasAds())
        {
            bannerView = new BannerView(BannerId, AdSize.Banner, AdPosition.Top);
            AdRequest request = new AdRequest.Builder().Build();
            bannerView.LoadAd(request);
        }
        //Debug.Log("Banner Loaded");
    }
    //---------------------------------------------------------------------------
    void LoadInterstitial()
    {
        if (HasAds())
        {
            interstitial = new InterstitialAd(InterstitialId);
            AdRequest request = new AdRequest.Builder().Build();
            interstitial.LoadAd(request);
        }
    }
    //---------------------------------------------------------------------------
    void LoadStaticInterstitial()
    {
        if (HasAds())
        {
            staticInterstitial = new InterstitialAd(StaticInterstitialId);
            AdRequest request = new AdRequest.Builder().Build();
            staticInterstitial.LoadAd(request);
        }
    }
    //---------------------------------------------------------------------------
    public void LoadVideo()
    {        
        AdRequest request = new AdRequest.Builder().Build();
        rewardBasedVideo.LoadAd(request, VideoId);
    }
    //---------------------------------------------------------------------------
    public void ShowInterstitial()
    {
        if (!HasAds())
            return;
        //Debug.Log("ShowInter");
        if (interstitial.IsLoaded())
        {
            interstitial.Show();
        }
        LoadInterstitial();
    }
    //---------------------------------------------------------------------------
    public void ShowStaticInterstitial()
    {
        if (!HasAds())
            return;

        if (staticInterstitial.IsLoaded())
        {
            staticInterstitial.Show();
        }
        LoadStaticInterstitial();
    }
    //---------------------------------------------------------------------------
    public void RemoveBanner()
    {
        if (bannerView != null)
        {
            bannerView.Hide();
            bannerView.Destroy();
        }
    }
    //---------------------------------------------------------------------------
    int VideoPurpose = 0;

    public void ShowRewardBasedVideo(int purpose) //1 for coins, 2 for revive, 3 for 2x coins
    {
        if (RewardBasedVideoAd.Instance.IsLoaded())
        {
            if (purpose == 1)
            {
               
            }
            else if (purpose == 2)
            {
//                videobtnRevive.SetActive(false);
            }
            else if (purpose == 3)
            {
                JustWatchedPlus100 = true;
            }
            homeVidBtn.SetActive(false);
            x2Btn.SetActive(false);
            VideoPurpose = purpose;
            RewardBasedVideoAd.Instance.Show();
        }
        else
        {
            LoadVideo();
            //Debug.Log("No video currently available. Please try again later.");
        }

    }
    //---------------------------------------------------------------------------
    public void HandleRewardBasedVideoRewarded(object sender, Reward args)
    {
        string type = args.Type;
        double amount = args.Amount;

        if (VideoPurpose == 1)
        {            
            doubleCoins = true;
            x2Icon.SetActive(true);


//            HomeCtrl.GetInstanse().OnRewardedVideo1Watched();
        }
        else if (VideoPurpose == 2)
        {
//            HomeCtrl.GetInstanse().OnRewardedVideo2Watched();
           
            Revive();
        }
        else if (VideoPurpose == 3)
        {
			Flying100.SetActive(true);
        }

        VideoPurpose = 0;

        if (!IsRewardedVideoLoaded())
        {
            LoadVideo();
        }
    }
    //---------------------------------------------------------------------------
    public void HandleRewardBasedVideoLoaded(object sender, EventArgs args)
    {
        if (RewardBasedVideoAd.Instance.IsLoaded())
        {
          
            if (PlayMode == PlayMode.Classic && TotalGames > 0)
                x2Btn.SetActive(true);
            else
                x2Btn.SetActive(false);
         
            //HomeCtrl.GetInstanse().videobtnRevive.SetActive(false);
            if (doubleCoins || HasDoubleCoins()) // || HomeCtrl.GetInstanse().DoubleCoinsForOneGame
            {
                x2Btn.SetActive(false);
            }
            else
            {
//                HomeCtrl.GetInstanse().videobtnx2.SetActive(true);
            }

            if (!JustWatchedPlus100)
            {
                homeVidBtn.SetActive(true);
            }

        }
    }
    //---------------------------------------------------------------------------
    public bool HasAds()
    {
        return !noAdds;
    }
    //---------------------------------------------------------------------------
    public bool HasDoubleCoins()
    {
        return DoubleCoins == 1;
    }
    //---------------------------------------------------------------------------
    public bool DoubleCoinsForOneGame()
    {
//        return HomeCtrl.GetInstanse().DoubleCoinsForOneGame;
        return true;
    }
    //---------------------------------------------------------------------------
    public bool IsRewardedVideoLoaded()
    {
        return RewardBasedVideoAd.Instance.IsLoaded();
    }
    //---------------------------------------------------------------------------
}
