﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Security.Cryptography;
using System.Text;
using System.Resources;

public partial class Game : MonoBehaviour
{
    public static int hiScore = 0;
    public static int hiScoreExtr = 0;
    public static int currentScore = 0;
    public GameObject moneyAnim;
    public static string hiScoreId = "HiScore";
    const string salt = "asf34nkyA7s9d1";
    bool resetScore = false;
    IEnumerator moneyCr;

    public int PlayerScoreExtr
    {
        get
        {
            hiScoreExtr = PlayerPrefs.GetInt("HiScoreExtr", 0);
            return hiScoreExtr;
        }
        set
        {
            if (CheckHash())
            {
                hiScoreExtr = value;
                PlayerPrefs.SetInt("HiScoreExtr", hiScoreExtr);
                PlayerHash = PrepareForHash();
            }
            else
                ResetValues();
        }
    }

    public int PlayerScore
    {
        get
        {
            hiScore = PlayerPrefs.GetInt("HiScore", 0);
            return hiScore;
        }
        set
        {
            if (CheckHash())
            {
                if (Tutorial != 0)
                {
                    hiScore = value;
                    PlayerPrefs.SetInt("HiScore", hiScore);
                    PlayerHash = PrepareForHash();
                }
            }
            else
                ResetValues();
        }
    }

    int playerMoney;

    public int PlayerMoney
    {
        get
        {
            //PlayerPrefs.SetInt("Money", 500);
            playerMoney = PlayerPrefs.GetInt("Money", 0);
            return playerMoney;
        }
        set
        {
            if (CheckHash())
            {
                if (Tutorial != 0)
                {
                
                    playerMoney = value;
                    PlayerPrefs.SetInt("Money", playerMoney);
                    PlayerHash = PrepareForHash();
                }
                else if (UITitlePanel.activeSelf)
                {
                    playerMoney = value;
                    PlayerPrefs.SetInt("Money", playerMoney);
                    PlayerHash = PrepareForHash();
                }
            }
            else
                ResetValues();
        }
    }

    public string PlayerHash{ get { return PlayerPrefs.GetString("Hash", ""); } set { PlayerPrefs.SetString("Hash", value); } }

    public Text UIPlayerScore, UIHiScore;


    string PrepareForHash()
    {
        string toCalc = salt + PlayerMoney.ToString()
                        + "-" + PlayerScore.ToString() + "-" + PlayerScoreExtr.ToString() + "-" + DoubleCoins.ToString() + salt;
//        PlayerHash = CalculateMD5Hash(toCalc);
        return  CalculateMD5Hash(toCalc);
    }

    bool CheckHash()
    {
        string toCheck = salt + PlayerMoney.ToString()
                         + "-" + PlayerScore.ToString() + "-" + PlayerScoreExtr.ToString() + "-" + DoubleCoins.ToString() + salt;
        if (string.IsNullOrEmpty(PlayerHash))
        {
            PlayerPrefs.SetInt("Money", 0);  
            PlayerPrefs.SetInt("HiScore", 0);
            PlayerPrefs.SetInt("HiScoreExtr", 0);
            toCheck = salt + PlayerMoney.ToString()
            + "-" + PlayerScore.ToString() + "-" + PlayerScoreExtr.ToString() + "-" + DoubleCoins.ToString() + salt;
            PlayerHash = CalculateMD5Hash(toCheck);
            return true;
        }
        //Debug.Log(CalculateMD5Hash(toCheck));
        //Debug.Log(PlayerHash);
        return string.Equals(CalculateMD5Hash(toCheck), PlayerHash);
    }

    void ResetValues()
    {
        PlayerPrefs.SetInt("Money", 0);  
        PlayerPrefs.SetInt("HiScore", 0);
        PlayerPrefs.SetInt("HiScoreExtr", 0);
        PlayerPrefs.SetInt("DoubleCoins", 0);
        PlayerHash = PrepareForHash();
        UpdateScore();
    }

    public string CalculateMD5Hash(string input)
    {

        // step 1, calculate MD5 hash from input

        MD5 md5 = System.Security.Cryptography.MD5.Create();

        byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);

        byte[] hash = md5.ComputeHash(inputBytes);

        // step 2, convert byte array to hex string

        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < hash.Length; i++)
        {

            sb.Append(hash[i].ToString("X2"));

        }

        return sb.ToString();

    }

    public void CheckScoreGeneral()
    {
        if (PlayMode == PlayMode.Classic)
            CheckScore();
        else
            CheckScoreExtr();
    }

    void CheckScore()
    {
        if (PlayerScore < currentScore)
        {
            PlayerScore = currentScore;
            ReportScore(PlayerScore, 1);
        }
        UpdateScore();
    }

    void CheckScoreExtr()
    {
        if (PlayerScoreExtr < currentScore)
        {
            PlayerScoreExtr = currentScore;
            ReportScore(PlayerScoreExtr, 2);
        }
        UpdateScore();
    }

    public void UpdateScore()
    {
        UpdateScores();
        UIUpdateScoreHolders();
    }

    void UpdateScores()
    {
        UIPlayerScore.text = currentScore.ToString();
        if (PlayMode == PlayMode.Classic)
            UIHiScore.text = PlayerScore.ToString();
        else
            UIHiScore.text = PlayerScoreExtr.ToString();
    }

    public void AddMoney(int amount)
    {
        PlayerMoney += amount;
        UpdateScore();
    }

    public void AddScore(int score)
    {
        //if (!PauseMgr.GetInstanse().tutorialFinished)
        if (Tutorial != 0)
        {
           
            StartCoroutine(IAddScore(score));
        }
    }

    IEnumerator IAddScore(int score)
    {
        int step = (int)score / 3;
        int temp = score - step * 3;
        for (int i = 0; i < score - temp; i += step)
        {
            if (resetScore)
            {
                currentScore = 0;
                UpdateScore();
                break;
            }
            currentScore += step;
            UpdateScore();

            //            PowerUpInfo.CheckScore();
            //            ScoreCtrl.GetInstanse().UpdateHiScore();
            yield return new WaitForSeconds(0.10f);
        } 
        if (resetScore)
        {
            currentScore = 0;

           
        }
        else
        {
            currentScore += temp;
        }
        UpdateScore();
        resetScore = false;
       
        //        Debug.Log(PowerUpInfo.playerCurrentScore);
        yield return new WaitForSeconds(0.10f);
    }

    void CallAddMoney(int money)
    {
        if (Tutorial != 0)
        {
            if (doubleCoins)
                money *= 2;
            if (UIGamePanel.activeSelf && money > 0)
            {
//                moneyAnim.SetActive(false);
                moneyAnim.transform.GetChild(0).transform.GetChild(0).GetComponent<Text>().text = "+" + money.ToString();
                if (moneyCr != null)
                {
                    AddMoney(money);
//                    StopCoroutine(moneyCr);
                }
                else
                {
                    moneyCr = IMoneyAnim(money);
                    StartCoroutine(moneyCr);
                    
                }
//                moneyAnim.SetActive(false);
            }
            else
                StartCoroutine(IAddMoney(money));
        }
    }

   
    Vector3 startPos = Vector3.zero;

    IEnumerator IMoneyAnim(int money)
    {
        Vector3 endPos = GameMoney.transform.position;
        Vector3 viewportPoint = Camera.main.WorldToViewportPoint(startPos);
        RectTransform rt = moneyAnim.GetComponent<RectTransform>();
        //        rt.anchorMin = viewportPoint;  
        //        rt.anchorMax = viewportPoint; 
        moneyAnim.transform.position = startPos;
        moneyAnim.SetActive(true);
        //        Debug.Log("viewportPoint " + viewportPoint);
        //        Debug.Log("viewportPoint " + viewportPoint);


        while (Vector3.Distance(moneyAnim.transform.position, endPos) > 0.003f)
        {
            moneyAnim.transform.position = Vector3.MoveTowards(moneyAnim.transform.position, endPos, 2.5f);
            yield return new WaitForSeconds(0.005f);

        }
        

        StartCoroutine(IAddMoney(money));
        moneyAnim.SetActive(false);
        moneyCr = null;
    }

    IEnumerator IAddMoney(int money)
    {
        int step = (int)(money / 3);
        int temp = money - step * 3;
        for (int i = 0; i < money - temp; i += step)
        {
            AddMoney(step);
//            UpdateScore();

            //            PowerUpInfo.CheckScore();
            //            ScoreCtrl.GetInstanse().UpdateHiScore();
            yield return new WaitForSeconds(0.10f);
        } 
        AddMoney(temp);
//        UpdateScore();

        //        Debug.Log(PowerUpInfo.playerCurrentScore);
        yield return new WaitForSeconds(0.10f);
    }

}
