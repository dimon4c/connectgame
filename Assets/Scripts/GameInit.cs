﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml.Linq;

//using UnityEngine.PlaymodeTests;
using System.Diagnostics;
using UnityEngine.UI;

public partial class Game : MonoBehaviour
{
	void PrepareTextColors(string color)
	{
		Color c = StringToColor(color);
		x2Icon.GetComponent<Image>().color = c;
		pauseBtn.GetComponent<Image>().color = c;
		hammerCost.color = c;
		swapCost.color = c;
		removeValueCost.color = c;
		ChangeHoldersColor(c);
		//        UnityEngine.Debug.Log(c);

	}
	//-------------------------------------------------
	void CreateField(Point size)
	{

		cellToIgnore = 0;
		startTime = Time.time;



		moneyAnim.SetActive(false);
		ActivatePowerUp();
		currentScore = 0;
		UpdateScore();
		nextCells.SetActive(false);
		endPointMode = false;
		//        UIHolder.sortingOrder = 0;
		if (DoubleCoins == 1)
		{
			x2Icon.SetActive(true);
			doubleCoins = true;
			x2Btn.SetActive(false);
		}
		else
		{
			x2Btn.SetActive(true);
			x2Icon.SetActive(false);
			doubleCoins = false;
		}

		fieldSize.X = size.X;
		fieldSize.Y = size.Y;
		float offX = -2.8f;
		float offY = -4f;

		float dx = 1f;
		float dy = 1.2f;


		if (PlayMode == PlayMode.Classic || Tutorial == 0)
		{
			switch (CurrentSkin)
			{

			case 0:         //square
				offY = -4.2f;
				offX = -2.9f;
				dx = 1.06f;
				dy = 1.06f;
				CellPrefab.transform.localScale = new Vector3(0.68f, 0.68f, 1f);

				break;

			case 1:         //hex
				offY = -4.5f;
				dy = 1.15f;
				CellPrefab.transform.localScale = new Vector3(0.7f, 0.7f, 1f);
				break;

			case 2:         //sircle
				offY = -4.5f;
				dx = 0.99f;
				dy = 1.14f;
				//                    offX = -2.7f;
				CellPrefab.transform.localScale = new Vector3(0.68f, 0.68f, 1f);
				break;
			}
		}
		else
		{
			CellPrefab.transform.localScale = new Vector3(0.68f, 0.68f, 1f);
			switch (CurrentSkin)
			{
			case 0:
				CellPrefab.transform.localScale = new Vector3(0.67f, 0.67f, 1f);
				offY = -3.37f;
				offX = -2.87f;
				dx = 1.02f;
				dy = 1.04f;
				break;
			case 1:
				CellPrefab.transform.localScale = new Vector3(0.7f, 0.7f, 1f);
				offY = -3.8f;
				offX = -2.8f;
				dx = 1.01f;
				dy = 1.17f;
				break;
			case 2:
				offY = -3.8f;
				dy = 1.17f;
				dx = 1.005f;
				break;
			}


		}

		Field = new Cell[size.X, size.Y];
		cellToIgnore = 0;
		//Build cells
		for (int i = 0; i < size.X; i++)
		{
			for (int j = 0; j < size.Y; j++)
			{

				float offset = 0;
				if (i % 2 == 1)
				{
					offset = dy / 2f;

				}
				GameObject go = Instantiate(CellPrefab, new Vector2(offX + (i * dx), offY + (j * dy) + offset),
					Quaternion.identity) as GameObject;

				go.transform.SetParent(FieldParent.transform);
				//                go.transform.Rotate(0f, 0f, 30f);
				go.SetActive(true);
				go.name = "Cell: " + i + "_" + j;
				Cell goCell = go.GetComponent<Cell>();
				goCell.SetPos(i, j, size.X, size.Y, this);
				goCell.ResetValue();
				Field[i, j] = goCell;
				go.GetComponent<SpriteRenderer>().sprite = cellTypes[CurrentSkin];
				go.GetComponent<SpriteRenderer>().color = backCellsColor[CurrentBG];
				goCell.SetDefaultSprite(cellTypes[CurrentSkin]);
				goCell.SetDefaultColor();
				//                goCell.SetDefaultColor(Color.gray);
				/*/
                int randomColor;
                if (cellColors != null && cellColors.Length > 0)
                {
                    randomColor = Random.Range(0, cellColors.Length);
//                    go.GetComponent<SpriteRenderer>().material.color = cellColors[randomColor];
                    go.GetComponent<SpriteRenderer>().color = cellColors[randomColor];
                    goCell.colorId = randomColor;
                }
              //*/

				if (PlayMode == PlayMode.Extreme && i % 2 == 1 && j == size.Y - 1)
				{
					cellToIgnore++;
					goCell.SetValue(9999);
					goCell.interactable = false;
					go.SetActive(false);
				}
				if (PlayMode == PlayMode.Classic && i % 2 == 0 && j == 0)
				{
					cellToIgnore++;
					goCell.SetValue(9999);
					goCell.interactable = false;
					go.SetActive(false);
				}

			}
		}

		if (Tutorial == 0)
		{
			PrepareTutorialMap();
		}
		else
		{
			//set if not tutorial

			foreach (TextMesh m in spawnVal)
			{
				m.transform.parent.gameObject.GetComponent<SpriteRenderer>().sprite = usedCellType[CurrentSkin];
				int randomColor = Random.Range(0, cellColors.Length);
				m.transform.parent.gameObject.GetComponent<SpriteRenderer>().color = cellColors[randomColor];
			}
			if (string.IsNullOrEmpty(PlayerPrefs.GetString("GameState", "")))
			{
				if (PlayMode == PlayMode.Classic)
				{
					//                cellsToSet = 4;
					SetRandomCellsVal(14);
					GeneratePresets(cellsToSet);
				}
				else
				{
					SetRandomCellsVal(20);
					GeneratePresets(cellsToSet);
				}
			}

			/*/Build neihbords list
        for (int i = 0; i < size.X; i++)
        {
            for (int j = 0; j < size.Y; j++)
            {
                Cell cell = Field[i, j].GetComponent<Cell>();
                cell.Neighbords = new LinkedList<GameObject>();

                for (int n = i - 1; n <= i + 1; n++)
                {
                    for (int m = j - 1; m <= j + 1; m++)
                    {
                        if (n < 0 || n >= size.X || m < 0 || m >= size.Y)
                            continue;
                        cell.Neighbords.AddLast(Field[n, m]);
                    }
                }

            }
        }
        //*/
			if (!string.IsNullOrEmpty(PlayerPrefs.GetString("GameState", "")) && savedData != null)
			{
				LoadField();
			}
			UpdateScore();

		}
	}

	void LoadField()
	{
		//UnityEngine.Debug.Log("LoadField");
		GeneratePresets(cellsToSet);
		currentScore = savedData.score;

		for (int i = 0; i < fieldSize.X; i++)
		{
			for (int j = 0; j < fieldSize.Y; j++)
			{
				Field[i, j].ResetValue();
			}
		}
		foreach (CellsInfo c in savedData.usedCells)
		{
			Cell temp = Field[c.x, c.y];
			temp.used = true;
			temp.colorId = c.colorId;
			temp.gameObject.GetComponent<SpriteRenderer>().sprite = usedCellType[CurrentSkin];
			temp.gameObject.GetComponent<SpriteRenderer>().color = cellColors[c.colorId];
			temp.SetValue(c.value);
			if (temp.Value == 9999)
			{
				cellToIgnore++;
				temp.interactable = false;
				temp.gameObject.SetActive(false);
			}
		}
		PreparePowers(savedData);
		//        ActivatePowerUp();
		UpdatePrices(PowerUps.Hammer);
		UpdatePrices(PowerUps.Swap);
		UpdatePrices(PowerUps.RemoveValue);
	}
	//-------------------------------------------------

}
