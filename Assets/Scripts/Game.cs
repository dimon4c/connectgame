﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.EventSystems;
using UnityEngine.UI;
using GooglePlayGames.BasicApi.Multiplayer;
using System;

public partial class Game : MonoBehaviour
{
    //    bool homeVidSeen = false;
    //-------------------------------------------------
    //Public variables linked to the Unity Scene objects
    //    Camera cam;
    public GameObject CellPrefab, GameMoney;
    public GameObject firstGameAnim, homeVidAnim;
    public GameObject FieldParent;
    public GameObject UITitlePanel;
    public GameObject UIGamePanel;
    public GameObject UIGameOverPanel;
    public GameObject UIPausePanel;
    public GameObject UIShopPanel;
    public GameObject UIShopPanelContent;
    public GameObject UISkinPanel;
    public GameObject UIScorePanel;
    public GameObject UIPowersPanel;
    public GameObject nextCells;
    public Image UIBackground;
    public List<GameObject> skinObj;
    public List<GameObject> colorObj;
    public Canvas UIHolder;
    Image[] BGImages;
    public List<Sprite> cellTypes;
    public List<Sprite> usedCellType;
	public GameObject pauseBtn, pauseBtn2;
	public GameObject restorePurchasesBtn;
	public Text Cost2xStar;
	public GameObject Flying100;

	bool revived
	{ 
		get { return Convert.ToBoolean(PlayerPrefs.GetInt("playtime", 0)); }
		set
		{
			PlayerPrefs.SetInt("playtime", Convert.ToInt32(value)); 
		} 
	}
    public static string[] BaseSet =
        {
            "fcaf28",
            "38cefa",
            "c474f1",
            "12de34",
            "5c6fff",
            "eede29",
            "fd6363",
            "9051de",
            "e60cba",
            "ff7ae1",
            "aad626",
            "d28c1e",
            "008b45",
            "a9496e"
        };
    Color[] cellColors = new Color[BaseSet.Length];
    Color[] backCellsColor = new Color[]
    { 
        new Color(74f / 255f, 72f / 255f, 72f / 255f, 1f), //black
        new Color(94f / 255f, 94f / 255f, 94f / 255f, 1f), //gray
        new Color(134f / 255f, 172f / 255f, 147f / 255f, 1f), //green
        new Color(141f / 255f, 190f / 255f, 208f / 255f, 1f), //blue
        new Color(215f / 255f, 215f / 255f, 215f / 255f, 1f) //white
    };
    public static Game Instance;
    bool doubleCoins = false;
    public List<Text> hiScoreText, hiScoreExtrText, curScore, totalStars;
    public GameObject x2Btn, x2Icon;
    SaveData savedData;
    //-------------------------------------------------
    float startTime;
    //-------------------------------------------------
    //Variables used in Game class
    Cell[,] Field;
    public PlayMode PlayMode = PlayMode.Classic;
    //-------------------------------------------------
    public Camera gameCamera;

    public bool JustWatchedPlus100 = false;

    static Color StringToColor(string col)
    {
        Color c;
        if (!ColorUtility.TryParseHtmlString("#" + col + "FF", out c))
            c = Color.white;
        return c;
    }

    void PrepareColors()
    {
        for (int i = 0; i < BaseSet.Length; i++)
        {
            cellColors[i] = StringToColor(BaseSet[i]);
        }
    }

    void Awake()
    {
        
        
        if (Instance == null)
        {
            Instance = this;
        }

        PrepareColors();
        //PlayerPrefs.DeleteAll();
        hiScore = PlayerScore;
        UpdateScore();
        noAdds = RemoveAdds != 0;
        BGImages = GameObject.Find("Backgrounds").GetComponentsInChildren<Image>();
//        SetCurrentBG(3);
        SetChoosenBG();
        aSource = GetComponent<AudioSource>();
    }

   
    //-------------------------------------------------
    //Functions
    //-------------------------------------------------
    void Start()
    {
//        endPos = new Vector3();
      
//        PlayerPrefs.SetInt("HiScoreExtr", 9999);
        CheckDisable();
        CheckAudio();
        InitGameServices();
        InitAdmob();
        ShowTitleScreen();
        CheckSave();
//        Tutorial = 1;
        if (Tutorial == 0)
            StartTutorialMode();
        if (!CheckHash())
            ResetValues();
        switch (CurrentBG)
        {
            case 4:
                PrepareTextColors("a1a0a0");
                break;
            default:
                PrepareTextColors("");
                break;
        }
//        PlayerPrefs.SetInt("FirstStart", 0);

//        AddMoney(2000);

		#if UNITY_ANDROID
		restorePurchasesBtn.SetActive(false);
		Cost2xStar.text = "$2.49";
		#endif

		#if UNITY_IOS
		Cost2xStar.text = "$2.99";
		#endif

    }

    bool dialogQuitVisible = false;
    float lastOpenedTime = 0;

    private void OnDialogClose(MNDialogResult result)
    {
        //parsing result
        dialogQuitVisible = false;
        switch (result)
        {
            case MNDialogResult.YES:
                Application.Quit();
                break;
            case MNDialogResult.NO:
                break;
        }
    }

    bool dialogRateVisible = false;

    bool RateDialog()
    {
        if (dialogRateVisible)
        {
            return false;
        }



        float endTime = Time.time;
        float levelDur = endTime - startTime;
        TotalPlayTime += levelDur;
        bool shown = false;
//        Debug.Log("levelDur " + levelDur);
//        Debug.Log("TotalPlayTime " + TotalPlayTime);
        if (TotalGames >= 2)
        {
            if (PlayerPrefs.GetInt("rate", 0) == 0 && TotalPlayTime >= 10 * 60)
            {
                #if UNITY_EDITOR
                Debug.Log("RateDialog");
                #endif
                MobileNativeRateUs ratePopUp = new MobileNativeRateUs("Do you like this Game?", "Please take a moment to rate it. Thanks for your support!", "RATE", "LATER", "NO, THANKS");
                
                #if UNITY_ANDROID
                ratePopUp.SetAndroidAppUrl("https://play.google.com/store/apps/details?id=com.numbers.connect");
                #endif

                #if UNITY_IOS
				ratePopUp.SetAppleId ("1219803368");
                #endif

                ratePopUp.OnComplete += OnRatePopUpClose;
                ratePopUp.Start();
                shown = true;
                dialogRateVisible = true;
            }
            if (PlayerPrefs.GetInt("rate", 0) == 1 && TotalPlayTime >= 50 * 60)
            {
                PlayerPrefs.SetInt("rate", 0);
                TotalPlayTime = 0;
            }
        }
        return shown;
    }

    private void OnRatePopUpClose(MNDialogResult result)
    {
        dialogRateVisible = false;
        //parsing result
        switch (result)
        {
            case MNDialogResult.RATED:
                PlayerPrefs.SetInt("rate", 2);
                break;
            case MNDialogResult.REMIND:
                PlayerPrefs.SetInt("rate", 1);
                break;
            case MNDialogResult.DECLINED:
                PlayerPrefs.SetInt("rate", 2);
                break;
        }
    }

    //-------------------------------------------------
    // Update is called once per frame
    void Update()
    {
        //not used yet
        if (Input.anyKey)
        {
            DisablePowerAnim();
        }
        if (Input.GetKeyDown(KeyCode.Escape) == true)
        {
            if (UIPausePanel.activeSelf)
            {
                UIPausePanel.SetActive(false);
                SetAllCellsInteraction(true);
            }
            else if (!UIPausePanel.activeSelf && UIGamePanel.activeSelf && Tutorial != 0)
            {
                UIPausePanel.SetActive(true);
                SetAllCellsInteraction(false);
            }
            if (UIShopPanel.activeSelf)
            {
                UICloseShop();
            }
            if (UIGameOverPanel.activeSelf)
            {
                UIHomeFromOver();
            }
            if (UISkinPanel.activeSelf)
            {
                UIHideSkins();
            }

            if (UITitlePanel.activeSelf && !UISkinPanel.activeSelf && !UIShopPanel.activeSelf)
            {
               
                if (!dialogQuitVisible)
                {
                    if (lastOpenedTime == 0 || (Time.time - lastOpenedTime > 1f))
                    {
                        //Debug.Log ("test");
                        #if UNITY_EDITOR
                        Debug.Log("Quit popup");
                        #endif
                        dialogQuitVisible = true;
                        lastOpenedTime = Time.time;
                        MobileNativeDialog dialogQuit = new MobileNativeDialog("Quit", "Exit the game?");
                        dialogQuit.OnComplete += OnDialogClose;
                    }
                }
            }

        }
    }
    //-------------------------------------------------
    public void ChooseSkin(int id)
    {
        //Debug.Log("ChooseSkin");
        foreach (GameObject go in skinObj)
        {
            go.transform.localScale = new Vector3(0.5f, 0.5f, 1f);
        }
        skinObj[id].transform.localScale = new Vector3(1f, 1f, 1f);
        SetCurrentSkin(id);
    }

    public void ChooseColor(int id)
    {
        //Debug.Log("ChooseColor");
        foreach (GameObject go in colorObj)
        {
            go.transform.localScale = new Vector3(1f, 1f, 1f);
        }

        colorObj[id].transform.localScale = new Vector3(2f, 2f, 1f);
        SetCurrentBG(id);
        SetChoosenBG();
        switch (CurrentBG)
        {
            case 4:
                PrepareTextColors("a1a0a0");
                break;
            default:
                PrepareTextColors("");
                break;
        }
    }

    public  void OnPointerUp(GameObject data)
    {
        //Debug.Log("OnPointerUp called.");
        Vector3 v = new Vector3(0.25f, 0.25f, 0.25f);
        data.transform.GetChild(0).gameObject.transform.localScale += v;
        data.transform.GetChild(1).gameObject.transform.localScale += v;
    }

    public void OnPointerDown(GameObject data)
    {
        Vector3 v = new Vector3(0.25f, 0.25f, 0.25f);
        data.transform.GetChild(0).gameObject.transform.localScale -= v;
        data.transform.GetChild(1).gameObject.transform.localScale -= v;
        //Debug.Log("OnPointerDown called.");
    }

}
