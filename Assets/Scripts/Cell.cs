﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Xml.Linq;

public struct NODE
{
    public Point parent;
    public Point id;
    public  int level;
    //    public bool used;
};

public class Cell : MonoBehaviour
{
    public GameObject valHolder;
    public TextMesh cellValue;
    public int Value = 1;
    public int colorId;
    public bool used = false;
    public Point cellPos;
    public  List<Point> neighbours = new List<Point>();
    public Color cellDefault;
    public Sprite defaultSprite;
    Game gameScript;
    public bool interactable = true;

   

    public Animator anim;
    public GameObject hammerImg, swapImg;
    Vector3 defaultScale;

    void Awake()
    {
//        cellDefault = GetComponent<SpriteRenderer>().color;
//        defaultSprite = GetComponent<SpriteRenderer>().sprite;
        anim = GetComponent<Animator>();
        defaultScale = GetComponent<Transform>().localScale;
//        defaultScale += new Vector3(0, 0, 1f);
        //anim.Play("CellSpawn");
    }

    public void RestoreScale()
    {
        if (Value == 9999)
            return;
        GetComponent<Transform>().localScale = defaultScale;
        gameObject.SetActive(false);
        gameObject.SetActive(true);
       
    }

    public void SetDefaultSprite(Sprite sprite)
    {
        defaultSprite = sprite;
    }

    public void SetDefaultColor(Color color = default(Color))
    {
        if (color != default(Color))
            cellDefault = color;
        else
            cellDefault = GetComponent<SpriteRenderer>().color;
        
    }

    public void SetValue(int value)
    {
        Value = value;

       
        if (value < 10000)
        {
            cellValue.fontSize = 70;
        }
        if (value < 1000)
        {
            cellValue.fontSize = 85;
        }
        if (value < 100)
        {
            cellValue.fontSize = 115;
        }
        if (value < 10)
        {
            cellValue.fontSize = 125;
        }
        if (gameScript.PlayMode == PlayMode.Extreme)
        {
            cellValue.fontSize -= 5;
        }
        cellValue.text = Value.ToString();
        valHolder.SetActive(true);
        used = true;
        //*/
        switch (value)
        {
            case 2048:
                if (PlayerPrefs.GetInt("2048", 0) == 0)
                {
                    gameScript.ReportAchievments("CgkI0pqv1MINEAIQAQ");
                    PlayerPrefs.SetInt("2048", 1);
                }
                break;
            case 4096:
                if (PlayerPrefs.GetInt("4096", 0) == 0)
                {
                    gameScript.ReportAchievments("CgkI0pqv1MINEAIQAg");
                    PlayerPrefs.SetInt("4096", 1);
                }
                break;
            case 8192:
                if (PlayerPrefs.GetInt("8192", 0) == 0)
                {
                    gameScript.ReportAchievments("CgkI0pqv1MINEAIQAw");
                    PlayerPrefs.SetInt("8192", 1);
                }
                break;
            default:
                break;
        }
        //*/
        UnsetPressed();

    }

    public void HideValue()
    {
        valHolder.SetActive(false);
    }

    public void ResetSprite()
    {
        GetComponent<SpriteRenderer>().sprite = defaultSprite;
        GetComponent<SpriteRenderer>().color = cellDefault;
    }

    public void ResetValue()
    {
        if (Value == 9999)
        {
            return;
        }
        used = false;
        Value = 0;
        cellValue.text = Value.ToString();
        valHolder.SetActive(false);
        GetComponent<SpriteRenderer>().sprite = defaultSprite;
        GetComponent<SpriteRenderer>().color = cellDefault;
//        GetComponent<SpriteRenderer>().sprite = spritePool[gamescripr.CurrentBG];
        UnsetPressed();
    }

    public void SetPos(int x, int y, int gridX, int gridY, Game gamescript)
    {
        gameScript = gamescript;

        cellPos = new Point(x, y);

        FindNeighbours(gridX, gridY);
    }

    void FindNeighbours(int gridX, int gridY)
    {
        neighbours.Add(new Point(cellPos.X, cellPos.Y - 1));
        neighbours.Add(new Point(cellPos.X, cellPos.Y + 1));
        if (cellPos.X % 2 == 0)
        {
            neighbours.Add(new Point(cellPos.X + 1, cellPos.Y - 1));
            neighbours.Add(new Point(cellPos.X + 1, cellPos.Y));
            neighbours.Add(new Point(cellPos.X - 1, cellPos.Y));
            neighbours.Add(new Point(cellPos.X - 1, cellPos.Y - 1));
        }
        else
        {
            neighbours.Add(new Point(cellPos.X + 1, cellPos.Y + 1));
            neighbours.Add(new Point(cellPos.X + 1, cellPos.Y));
            neighbours.Add(new Point(cellPos.X - 1, cellPos.Y));
            neighbours.Add(new Point(cellPos.X - 1, cellPos.Y + 1));
        }
//        ShowNeighbours();
        List<Point> toRemove = new List<Point>();
        foreach (Point p in neighbours)
        {
            if (p.X < 0 || p.X >= gridX || p.Y < 0 || p.Y >= gridY)
                toRemove.Add(p);
        }
        foreach (Point p in toRemove)
        {
            neighbours.Remove(p);
        }
//        ShowNeighbours();
    }

    void OnMouseDown()
    {
//        gameScript.ShowNeighbours(cellPos);
        //*/
        if (gameScript.inProgress || gameScript.isAnimating)
            return;
       
        if (!gameScript.inProgress)
        {

            if (!interactable)
                return;

            switch (gameScript.enabledNow)
            {
                case PowerUps.None:
                    ChooseCell();
                    break;
                case PowerUps.Hammer:
                    UseHammer();
                    break;
                case PowerUps.Swap:
                    DoSwap();
                    break;
                case PowerUps.RemoveValue:
                    UseByValRemove();
                    break;

            }
            if (used)
                gameScript.PlaySound(0);

                
           
        }
        //*/
    }

    void SetPressed()
    {
        GetComponent<SpriteRenderer>().material.color = Color.gray;
    }

    public   void UnsetPressed()
    {
//        anim.Play("Cell");
        RestoreScale();
        GetComponent<SpriteRenderer>().material.color = Color.white;
    }

    void ChooseCell()
    {
        if (!gameScript.endPointMode && Value == 0)
            return;
        
        if (gameScript.startPoint.Equals(cellPos) && gameScript.endPointMode)
        {
            
            gameScript.endPointMode = false;
            //Debug.Log("Reset path");
            UnsetPressed();
        }
        else if (used)
        {
            
            gameScript.CallUnset();
            SetStartPoint();
            RestoreScale();
            anim.Play("CellPulse");
        }
        else if (!used && gameScript.endPointMode)
            SetEndPoint();
        
    }


    void SetStartPoint()
    {
//        Debug.Log("Set path");
        gameScript.IncorrectPathPopUp(false);
        gameScript.startPoint = cellPos;
        gameScript.endPointMode = true;
        if (gameScript.Tutorial == 0)
        {
            gameScript.TutorialUsedCellClick();
        }
        SetPressed();
    }

    void SetEndPoint()
    {
        

        {
            //Debug.Log("Set endp");
            gameScript.endPoint = cellPos;
            gameScript.TryToSetCell();
            
        }
    }

    void UseByValRemove()
    {
        if (!used)
        {
            //TODO: uncorrect move animation
            return;
        }
        gameScript.RemoveByValue(Value);
    }

    public void EnableSwapMode()
    {
       
        swapImg.SetActive(true);


    }

    public void DisableSwapMode()
    {
        //TODO: here should be some animation/color restore action
        swapImg.SetActive(false);


    }

    void DoSwap()
    {
        if (!used)
        {
            //TODO: uncorrect move animation
            return;
        }
        //TODO: here should be some animatiom/color change etc.
        SetPressed();
        gameScript.SwapCells(this);
    }



    public void EnableHammer()
    {
        
        hammerImg.SetActive(true);
    }

    public void DisableHammer()
    {
        
        hammerImg.SetActive(false);
    }

    void UseHammer()
    {
        if (!used)
        {
            //TODO: uncorrect move animation
            return;
        }
        DisableHammer();
        Color cellCol = GetComponent<SpriteRenderer>().color;
        gameScript.ShowHammerAnim(gameObject.transform.position, cellCol);
        ResetValue();
        gameScript.DisableHammer(true);

    }

    void ShowNeighbours()
    {
        //Debug.Log("Cell " + cellPos.X + "_" + cellPos.Y);
        foreach (Point p in neighbours)
        {
            //Debug.Log("X " + p.X + " " + "y: " + p.Y);
        }
    }
}

