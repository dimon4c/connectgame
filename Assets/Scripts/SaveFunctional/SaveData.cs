﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class SaveData
{
    public int score;
    public int gameType;
    public List<int> savedPowerUps;
    public List<CellsInfo> usedCells;

}
