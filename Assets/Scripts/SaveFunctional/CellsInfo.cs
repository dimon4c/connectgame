﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class CellsInfo
{
    public int x;
    public int y;
    public int value;
    public int colorId;
}

