﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Diagnostics;
using UnityEngine.UI;
using System.IO;

public partial class Game : MonoBehaviour
{

    public GameObject homeVidBtn;

    //-------------------------------------------------
    void ShowTitleScreen()
    {
        homeVidBtn.SetActive(false);
        JustWatchedPlus100 = false;

        LoadVideo();

        string game = PlayerPrefs.GetString("GameState", "");
        if (string.IsNullOrEmpty(game))
        {
            bool shown = RateDialog();

            if (!shown)
            {
                if (TotalGames > 0)
                {
                    ShowInterstitial();
                }
            }
        }
         
        UITitlePanel.SetActive(true);
        UIGamePanel.SetActive(false);
        UIPausePanel.SetActive(false);
    }
    //-------------------------------------------------
    void ShowGameScreen()
    {
        LoadVideo();
        UITitlePanel.SetActive(false);
        UIGamePanel.SetActive(true);
        if (Tutorial == 1 && TotalGames == 1)
        {
            UIScorePanel.SetActive(true);
            UIPowersPanel.SetActive(true);
        }
//        UpdateScores();
    }
    //-------------------------------------------------
    void StartTutorialMode()
    {
//        AddMoney(300);
        UIScorePanel.SetActive(false);
        UIPowersPanel.SetActive(false);
        PlayMode = PlayMode.Classic;
        StartClassicMode();
        ShowGameScreen();
        nextCells.SetActive(false);
        pauseBtn.SetActive(false);
		pauseBtn2.SetActive(false);

    }

    void FirstGameAnim()
    {
        firstGameAnim.SetActive(true);
    }

    void StartClassicMode()
    {
        
        cellsToSet = Settings.cellsToSetClassic;
        CreateField(Settings.FieldSizeClassic);
        gameCamera.transform.position = new Vector3(-0.8f, 0.25f, -10f);
        gameCamera.orthographicSize = 6.5f;
        nextCells.transform.localPosition = new Vector3(258f, 683f, -1f);
        if (TotalGames > 0 && rewardBasedVideo.IsLoaded())
            x2Btn.SetActive(true);
        else
            x2Btn.SetActive(false);
//        nextCells.SetActive(true);
        //todo...
    }
    //-------------------------------------------------
    void StartExtremeMode()
    {
        cellsToSet = Settings.cellsToSetExtreme;
        CreateField(Settings.FieldSizeExtreme);
        gameCamera.transform.position = new Vector3(0.2f, 0.5f, -10f);
        gameCamera.orthographicSize = 7f;
        nextCells.SetActive(false);
        x2Btn.SetActive(false);
//        nextCells.transform.localPosition = new Vector3(232f, 888f, -1f);
        //todo...
    }
    //-------------------------------------------------
    public void UIPause()
    {
        SetAllCellsInteraction(false);
//        UIHolder.sortingOrder = 1;
        UIPausePanel.SetActive(true);
    }
    //-------------------------------------------------
    public void UIStartClassic()
    {
        if (Tutorial == 1 && TotalGames == 0 && string.IsNullOrEmpty(PlayerPrefs.GetString("GameState", "")))
        {
            FirstGameAnim();
        }
        if (string.IsNullOrEmpty(PlayerPrefs.GetString("GameState", "")))
            TotalGames++;
        PlayMode = PlayMode.Classic;
        StartClassicMode();
        ShowGameScreen();
    }
    //-------------------------------------------------
    public void UIStartExtreme()
    {
        if (Tutorial == 1 && TotalGames == 0 && string.IsNullOrEmpty(PlayerPrefs.GetString("GameState", "")))
        {
            FirstGameAnim();
        }
        if (string.IsNullOrEmpty(PlayerPrefs.GetString("GameState", "")))
            TotalGames++;
    

        PlayMode = PlayMode.Extreme;
        StartExtremeMode();
        ShowGameScreen();
    }
    //-------------------------------------------------
    public void UIHomeFromPause()
    {
//        UIHolder.sortingOrder = 1;
        CheckScoreGeneral();
        DeleteSave();
        //todo... invoke this from pause panel - but for now we will just move back home
        //todo... destroy all game objects
        ResetLevel();
        ShowTitleScreen();
    }
    //-------------------------------------------------
    public void UIContinueFromPause()
    {
        SetAllCellsInteraction(true);
//        UIHolder.sortingOrder = 0;
        UIPausePanel.SetActive(false);
    }
    //-------------------------------------------------
    public void UIRestart()
    {
        CheckScoreGeneral();
        currentScore = 0;
        UpdateScore();
//        UIHolder.sortingOrder = 0;

        DeleteSave();
        ResetLevel();
        switch (PlayMode)
        {
            case PlayMode.Classic:
                UIStartClassic();
                break;
            case PlayMode.Extreme:
                UIStartExtreme();
                break;
            default:
                UIHomeFromPause(); //just go home if incorrect case
                break;
        }

        UIPausePanel.SetActive(false);
        UIGameOverPanel.SetActive(false);
        if (Tutorial == 1 && TotalGames == 0)
        {
            FirstGameAnim();
        }
        TotalGames++;
    }

    public void UIShowGameOver()
    {
        SetAllCellsInteraction(false);
        if (!revived && TotalGames > 0)
        {
            ShowStaticInterstitial();
        }

        CheckScoreGeneral();

//        UIHolder.sortingOrder = 1;
        DeleteSave();
        UIGameOverPanel.SetActive(true);
        if (revived || TotalGames == 0)
            UIHomeFromOver();
    }

    public void UIHomeFromOver()
    {
//        UIHolder.sortingOrder = 1;
        //todo... invoke this from pause panel - but for now we will just move back home
        //todo... destroy all game objects
        ResetLevel();
        ShowTitleScreen();
        UIGameOverPanel.SetActive(false);
    }

    public void ShowLBoard()
    {
        ShowLeaderboard();
    }

    public void ReviveForStars()
    {
        if (PlayerMoney >= 700 && !revived)
        {
            PlayerMoney -= 700;
            Revive();
        }
    }

    public void ReviveForVideo()
    {
        if (!revived)
            ShowRewardBasedVideo(2);
    }


    public void UIShowSkins()
    {
        foreach (GameObject go in colorObj)
        {
            go.transform.localScale = new Vector3(1f, 1f, 1f);
        }
        foreach (GameObject go in skinObj)
        {
            go.transform.localScale = new Vector3(0.5f, 0.5f, 1f);
        }
        skinObj[CurrentSkin].transform.localScale = new Vector3(1f, 1f, 1f);
        colorObj[CurrentBG].transform.localScale = new Vector3(2f, 2f, 1f);
        UISkinPanel.SetActive(true);
        UITitlePanel.SetActive(false);
    }

    public void UIHideSkins()
    {
        UISkinPanel.SetActive(false);
        UITitlePanel.SetActive(true);

    }

    public void SetChoosenBG()
    {
        UIBackground.sprite = BGImages[CurrentBG].sprite;
    }

    void ChangeHoldersColor(Color c)
    {
        foreach (Text t in hiScoreText)
        {
            t.color = c;
        }
        foreach (Text t in hiScoreExtrText)
        {
            t.color = c;
        }
        foreach (Text t in curScore)
        {
            t.color = c;
        }
        foreach (Text t in totalStars)
        {
            t.color = c;
        }
        UIPlayerScore.color = c;
        UIHiScore.color = c;
    }


    void UIUpdateScoreHolders()
    {
//        hiScore, hiScoreExtr, curScore;
        foreach (Text t in hiScoreText)
        {
            t.text = PlayerScore.ToString();
        }
        foreach (Text t in hiScoreExtrText)
        {
            t.text = PlayerScoreExtr.ToString();
        }
        foreach (Text t in curScore)
        {
            t.text = currentScore.ToString();
        }
        foreach (Text t in totalStars)
        {
            t.text = PlayerMoney.ToString();
        }
    }

    public void UIOpenShop()
    {
        UIShopPanel.SetActive(true);
        if (DoubleCoins == 1)
        {
            UIShopPanelContent.transform.GetChild(1).gameObject.SetActive(false);
        }
        else
        {
            UIShopPanelContent.transform.GetChild(1).gameObject.SetActive(true);
        }
        if (RemoveAdds == 1)
        {
            UIShopPanelContent.transform.GetChild(0).gameObject.SetActive(false);
        }
        else
        {
            UIShopPanelContent.transform.GetChild(0).gameObject.SetActive(true);
        }
        if (UIGamePanel.activeSelf)
        {
            SetAllCellsInteraction(false);
        }
    }

    public void UICloseShop()
    {
        UIShopPanel.SetActive(false);
        if (UIGamePanel.activeSelf)
        {
            SetAllCellsInteraction(true);
        }
    }
}
