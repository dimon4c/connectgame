﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class Game : MonoBehaviour
{
    public bool noAdds;

    public int RemoveAdds    { get { return PlayerPrefs.GetInt("RemoveAdds", 0); } set { PlayerPrefs.SetInt("RemoveAdds", value); } }
}
