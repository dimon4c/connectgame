﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class Game : MonoBehaviour
{
    public int CurrentSkin    { get { return PlayerPrefs.GetInt("CurrentSkin", 1); } set { PlayerPrefs.SetInt("CurrentSkin", value); } }

    public int CurrentBG    { get { return PlayerPrefs.GetInt("CurrentBG", 4); } set { PlayerPrefs.SetInt("CurrentBG", value); } }

    public int SoundOn    { get { return PlayerPrefs.GetInt("SoundOn", 0); } set { PlayerPrefs.SetInt("SoundOn", value); } }

    public int Tutorial    { get { return PlayerPrefs.GetInt("Tutorial", 0); } set { PlayerPrefs.SetInt("Tutorial", value); } }

    public int TotalGames    { get { return PlayerPrefs.GetInt("TotalGames", 0); } set { PlayerPrefs.SetInt("TotalGames", value); } }

    public float TotalPlayTime { get { return PlayerPrefs.GetFloat("playtime", 0); } set { PlayerPrefs.SetFloat("playtime", value); } }

    public int DoubleCoins
    { 
        get
        {
            //            PlayerPrefs.SetInt("Money", 1000);
            int doubleCoins = PlayerPrefs.GetInt("DoubleCoins", 0);
            return doubleCoins;
        }
        set
        {
            if (CheckHash())
            {
                if (Tutorial != 0)
                {

                    int doubleCoins = value;
                    PlayerPrefs.SetInt("DoubleCoins", doubleCoins);
                    PlayerHash = PrepareForHash();
                }
            }
            else
                ResetValues();
        }

    }

    public void OnDoubleCoinsPurchased()
    {
        DoubleCoins = 1;
    }

    void SetCurrentBG(int id)
    {
        CurrentBG = id;
    }

    void SetCurrentSkin(int id)
    {
        CurrentSkin = id;
    }

    bool CheckSave()
    {
        string game = PlayerPrefs.GetString("GameState", "");
        if (!string.IsNullOrEmpty(game))
        {
            savedData = new SaveData();
            JsonUtility.FromJsonOverwrite(game, savedData);

//            PreparePowers(savedData);
//            Debug.Log(savedData.gameType);
            if (savedData.gameType == (int)PlayMode.Classic)
                UIStartClassic();
            else
                UIStartExtreme();

			return true;
//            LoadSaveGame();
        }
		return false;
        //        StartCoroutine(LoadSave());
    }

    List<Cell> GetUsedCells()
    {
        List<Cell> usedCells = new List<Cell>();
        for (int i = 0; i < fieldSize.X; i++)
        {
            for (int j = 0; j < fieldSize.Y; j++)
            {
                if (Field[i, j].used)
                {
                    usedCells.Add(Field[i, j]);
                }
            }
        }
        return usedCells;
    }

    void PreparePowers(SaveData loaded)
    {
//        activated = new List<PowerUps>();
        for (int i = 0; i < loaded.savedPowerUps.Count; i++)
        {
            powerCost[i] = loaded.savedPowerUps[i];
        }
        //        PowerUpsManager.Instance.ShowSaved();
        //        PowerUpActivator.GetInstanse().CheckPowerUps();
    }

    List<CellsInfo> GetCells()
    {
        List<CellsInfo> savedCells = new List<CellsInfo>();
        List<Cell> usedCells = GetUsedCells();
        foreach (Cell c in usedCells)
        {
            CellsInfo cell = new CellsInfo();
            cell.x = c.cellPos.X;
            cell.y = c.cellPos.Y;
            cell.value = c.Value;
            cell.colorId = c.colorId;
            savedCells.Add(cell);

        }
        return savedCells;
    }

    List<int> GetPowerUps()
    {
        List<int> savedPowerUps = new List<int>(powerCost);
       
        return savedPowerUps;
    }

    public void DeleteSave()
    {
        PlayerPrefs.SetString("GameState", "");
        /*/
        PlayerPrefs.SetInt("Save", 0);
        string path = Application.persistentDataPath + "/Save.json";
        Debug.Log(path);
        if (File.Exists(path))
            File.Delete(path);
        //*/
    }

    public SaveData PrepareSave()
    {
        SaveData newSave = new SaveData();
        newSave.score = currentScore;
        newSave.gameType = (int)PlayMode;
        newSave.usedCells = GetCells();
        newSave.savedPowerUps = GetPowerUps();
        return newSave;
    }

    public void CreateSave(SaveData newSave)
    {
        /*/
        string path = Application.persistentDataPath + "/Save.json";
        if (File.Exists(path))
            File.Delete(path);
      
        Debug.Log(json);
        File.WriteAllText(path, json);
        //*/
        if (Tutorial == 0)
            return;
        string json = JsonUtility.ToJson(newSave);
        PlayerPrefs.SetString("GameState", json);
        //        PlayerPrefs.SetInt("Save", 1);
    }

    public void CallCreateSave()
    {
        CreateSave(PrepareSave());
    }

    void OnApplicationQuit()
    {
        if (UIGamePanel.activeSelf && !UIGameOverPanel.activeSelf)
            CallCreateSave();

    }

    void OnApplicationPause(bool pauseStatus)
    {
        if (pauseStatus)
        {
            //android onPause()
            if (UIGamePanel.activeSelf && !UIGameOverPanel.activeSelf)
                CallCreateSave();
        }
        else
        {
            //android onResume()
        }
    }
}
