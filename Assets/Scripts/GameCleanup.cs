﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class Game : MonoBehaviour
{

    void ResetLevel()
    {
        revived = false;
        foreach (Transform child in FieldParent.transform)
        {
            GameObject.Destroy(child.gameObject);
        }
    }
}
