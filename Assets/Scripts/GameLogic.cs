﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public partial class Game : MonoBehaviour
{
    public GameObject incorrectMove;
    public bool endPointMode = false;
    public bool inProgress = false;
    public Point startPoint, endPoint;
    public Point fieldSize;
    int cellsToSet = 0;
    List<Point> cellsToClear;
    //minimalCells needed to merge, except current move's cell
    int minToclear = 4;
    public GameObject hand, tutorialMask, merge;
    public TextMesh[] spawnVal;
    int[] presetValues;
    bool wrongPathUsed = false;
    int CurrentTutorialStep = 0;
    int chainMerge = 0;
    public bool isAnimating = false;
    List<int> tutorialValues1 = new List<int> { 2, 2, 2, 2 };
    List<int> tutorialValues2 = new List<int>{ 2, 2, 4, 8 };
    List<int> tutorialValues3 = new List<int> { 8, 8, 8, 5 };
    //First position represents interactable cell
    List<Point> tutorialPositions1 = new List<Point> { new Point(0, 1), new Point(0, 3), new Point(0, 4), new Point(3, 5) };
    List<Point> tutorialPositions2 = new List<Point> { new Point(3, 3), new Point(3, 0), new Point(3, 1), new Point(4, 2) };
    List<Point> tutorialPositions3 = new List<Point> { new Point(0, 0), new Point(3, 5), new Point(3, 3), new Point(2, 4) };
    float blockTime;
    int cellToIgnore;
    List<Point> toSetPos = new List<Point>
    {
        new Point(1, 3),
        new Point(1, 2),
        new Point(1, 3),
    };
    int totalMoney = 0;
    int multiplier = 0;

    void ShowMerge(Cell cell)
    {
        Vector3 position = cell.gameObject.transform.position;
        merge.GetComponent<SpriteRenderer>().color = cell.GetComponent<SpriteRenderer>().color;
        merge.SetActive(false);
        merge.transform.position = position;
        merge.SetActive(true);
    }

    void PrepareTutorialMap()
    {
        SetAllCellsInteraction(false);
        SetConcreteCells(tutorialValues1, tutorialPositions1);
        SetCellInteraction(tutorialPositions1[0], true);
        hand.transform.position = Field[tutorialPositions1[0].X, tutorialPositions1[0].Y].gameObject.transform.position;
        hand.SetActive(true);
        tutorialMask.transform.position = hand.transform.position;
        tutorialMask.SetActive(true);
        SetCellInteraction(toSetPos[CurrentTutorialStep], true);

    }

    void TutorialStepManager()
    {
        CurrentTutorialStep++;
        //Debug.Log("TutorialStepManager " + CurrentTutorialStep);

        switch (CurrentTutorialStep)
        {
            case 1:
			//                ResetAllCells();
                SetAllCellsInteraction(false);
                SetConcreteCells(tutorialValues2, tutorialPositions2);
                SetCellInteraction(tutorialPositions2[0], true);
                hand.transform.position = Field[tutorialPositions2[0].X, tutorialPositions2[0].Y].gameObject.transform.position;
                hand.SetActive(true);
                tutorialMask.transform.position = hand.transform.position;
                tutorialMask.SetActive(true);
                SetCellInteraction(new Point(4, 1), true); //for blocked Pos
                SetCellInteraction(toSetPos[CurrentTutorialStep], true);
                break;
            case 2:
			//                ResetAllCells();
                EndTutorial();
			/*/
                SetAllCellsInteraction(false);
                SetConcreteCells(tutorialValues3, tutorialPositions3);
                SetCellInteraction(tutorialPositions3[0], true);
                SetCellInteraction(toSetPos[CurrentTutorialStep], true);
                //*/
                break;
            case 3:
			//Debug.Log("EndTutorial");
                EndTutorial();
                break;
            default:
			//Debug.Log("Smth went wrong");
                break;
        }
    }

    void TutorialWrongPath()
    {
        wrongPathUsed = true;
        hand.transform.position = Field[tutorialPositions2[0].X, tutorialPositions2[0].Y].gameObject.transform.position;
        hand.SetActive(true);
        tutorialMask.transform.position = hand.transform.position;
        tutorialMask.SetActive(true);
        //        TutorialUsedCellClick();
    }

    public void TutorialUsedCellClick()
    {
        if (CurrentTutorialStep != 1)
        {

            hand.transform.position = Field[toSetPos[CurrentTutorialStep].X, toSetPos[CurrentTutorialStep].Y].gameObject.transform.position;
            hand.SetActive(true);

        }
        else
        {
            if (!wrongPathUsed)
            {

                hand.transform.position = Field[4, 1].gameObject.transform.position;
                hand.SetActive(true);
            }
            else
            {
                hand.transform.position = Field[toSetPos[CurrentTutorialStep].X, toSetPos[CurrentTutorialStep].Y].gameObject.transform.position;
                hand.SetActive(true);
            }
        }
        tutorialMask.transform.position = hand.transform.position;
        tutorialMask.SetActive(true);
    }

    void SetConcreteCells(List<int> values, List<Point> positions)
    {
        for (int i = 0; i < values.Count; i++)
        {
            Cell cell = Field[positions[i].X, positions[i].Y];
            cell.SetValue(values[i]);

            if (cellColors != null && cellColors.Length > 0)
            {

                //                    go.GetComponent<SpriteRenderer>().material.color = cellColors[randomColor];
                cell.gameObject.GetComponent<SpriteRenderer>().sprite = usedCellType[CurrentSkin];


                cell.colorId = SetCorrespondingColor(cell.gameObject.GetComponent<SpriteRenderer>(), values[i]);
                SpriteRenderer sp = cell.gameObject.GetComponent<SpriteRenderer>();

            }
            if (CheckConnectedCells(cell.cellPos))
            {
                ClearCells(cell.cellPos);
            }

        }

    }

    void SetCellInteraction(Point cellPos, bool interactable)
    {
        if (Field[cellPos.X, cellPos.Y].Value != 9999)
            Field[cellPos.X, cellPos.Y].interactable = interactable;

    }


    void ResetAllCells()
    {
        for (int i = 0; i < fieldSize.X; i++)
        {
            for (int k = 0; k < fieldSize.Y; k++)
            {
                Field[i, k].ResetValue();
            }
        }
    }

    void SetAllCellsInteraction(bool interactable)
    {
        for (int i = 0; i < fieldSize.X; i++)
        {
            for (int k = 0; k < fieldSize.Y; k++)
            {
                SetCellInteraction(new Point(i, k), interactable);
            }
        }
    }

    void EndTutorial()
    {
        //end tutorial animation
        Tutorial = 1;
        hand.SetActive(false);
        tutorialMask.SetActive(false);
        SetAllCellsInteraction(true);
        SetRandomCellsVal(cellsToSet);
        GeneratePresets(cellsToSet);
        UIScorePanel.SetActive(true);
        x2Btn.SetActive(false);
        //        UIPowersPanel.SetActive(true);
        pauseBtn.SetActive(true);
        pauseBtn2.SetActive(true);

        //        nextCells.SetActive(true);
    }



    public IEnumerator MoveCell(List<Point> positions, GameObject objToCopy = null)
    {
        Transform toWork;
        if (objToCopy == null)
        {
            GameObject go = Instantiate(CellPrefab);
            toWork = go.transform;
        }
        else
        {
            GameObject go = Instantiate(objToCopy);
            if (go.GetComponent<CircleCollider2D>() != null)
                Destroy(go.GetComponent<CircleCollider2D>());
            toWork = go.transform;
        }
        Field[startPoint.X, startPoint.Y].ResetSprite();
        toWork.position = new Vector3(toWork.position.x, toWork.position.y, -5f);
        ParticleSystem part = toWork.gameObject.GetComponent<ParticleSystem>();
        part.startColor = cellColors[Field[startPoint.X, startPoint.Y].colorId];
        //Debug.Log(part == null);
        part.Play();
        Field[startPoint.X, startPoint.Y].HideValue();
        foreach (Point p in positions)
        {
            Vector3 pos = Field[p.X, p.Y].gameObject.transform.position;
            pos.z = -5f;
            while (Vector3.Distance(toWork.position, pos) > 0.3f)
            {
                toWork.position = Vector3.MoveTowards(toWork.position, pos, 0.3f);
                yield return new WaitForSeconds(0.03f);

            }
        }

        StartCoroutine(CheckMerge());

        Destroy(toWork.gameObject);
    }

    public IEnumerator MoveCellSwap(Point position1, Point position2, GameObject objToCopy1, GameObject objToCopy2)
    {
        SetAllCellsInteraction(false);
        Transform toWork1, toWork2;
        int val1 = objToCopy1.GetComponent<Cell>().Value;
        int val2 = objToCopy2.GetComponent<Cell>().Value;
        GameObject go1 = Instantiate(objToCopy1);
        GameObject go2 = Instantiate(objToCopy2);
        if (go1.GetComponent<CircleCollider2D>() != null)
            Destroy(go1.GetComponent<CircleCollider2D>());
        if (go2.GetComponent<CircleCollider2D>() != null)
            Destroy(go2.GetComponent<CircleCollider2D>());
        toWork1 = go1.transform;
        toWork2 = go2.transform;

        Field[position1.X, position1.Y].HideValue();
        Field[position2.X, position2.Y].HideValue();


        Field[position1.X, position1.Y].UnsetPressed();
        Field[position2.X, position2.Y].UnsetPressed();
        Field[position1.X, position1.Y].ResetSprite();
        Field[position2.X, position2.Y].ResetSprite();
        toWork1.position = new Vector3(toWork1.position.x, toWork1.position.y, -5f);
        ParticleSystem part1 = toWork1.gameObject.GetComponent<ParticleSystem>();
        part1.startColor = cellColors[Field[position1.X, position1.Y].colorId];
        //        Debug.Log(part == null);
        part1.Play();
        Field[position2.X, position2.Y].HideValue();

        toWork2.position = new Vector3(toWork2.position.x, toWork2.position.y, -5f);
        ParticleSystem part2 = toWork2.gameObject.GetComponent<ParticleSystem>();
        part2.startColor = cellColors[Field[position2.X, position2.Y].colorId];
        //        Debug.Log(part == null);
        part2.Play();
        Field[position2.X, position2.Y].HideValue();


        Vector3 pos2 = Field[position2.X, position2.Y].gameObject.transform.position;
        pos2.z = -5f;
        Vector3 pos1 = Field[position1.X, position1.Y].gameObject.transform.position;
        pos1.z = -5f;
        while (Vector3.Distance(toWork1.position, pos2) > 0.3f)
        {
            toWork1.position = Vector3.MoveTowards(toWork1.position, pos2, 0.3f);
            toWork2.position = Vector3.MoveTowards(toWork2.position, pos1, 0.3f);
            yield return new WaitForSeconds(0.01f);

        }


        Destroy(toWork1.gameObject);
        Destroy(toWork2.gameObject);

        Field[position2.X, position2.Y].SetValue(val1);

        if (cellColors != null && cellColors.Length > 0)
        {
            //            randomColor = Random.Range(0, cellColors.Length);
            //                    go.GetComponent<SpriteRenderer>().material.color = cellColors[randomColor];
            Field[position2.X, position2.Y].gameObject.GetComponent<SpriteRenderer>().sprite = usedCellType[CurrentSkin];

            Field[position2.X, position2.Y].colorId = SetCorrespondingColor(Field[position2.X, position2.Y].gameObject.GetComponent<SpriteRenderer>(), val1);
        }

        Field[position1.X, position1.Y].SetValue(val2);

        if (cellColors != null && cellColors.Length > 0)
        {
            //            randomColor = Random.Range(0, cellColors.Length);
            //                    go.GetComponent<SpriteRenderer>().material.color = cellColors[randomColor];
            Field[position1.X, position1.Y].gameObject.GetComponent<SpriteRenderer>().sprite = usedCellType[CurrentSkin];

            Field[position1.X, position1.Y].colorId = SetCorrespondingColor(Field[position1.X, position1.Y].gameObject.GetComponent<SpriteRenderer>(), val2);
        }


        //*/
        if (CheckConnectedCells(position1))
        {


            multiplier++;
            ClearCells(position1);

            if (CheckConnectedCells(position1))
            {
                do
                {
                    multiplier++;
                    ClearCells(position1);
                } while(CheckConnectedCells(position1));
            }
            MoneyControl();

            ShowMerge(Field[position1.X, position1.Y]);
        }
        if (CheckConnectedCells(position2))
        {


            multiplier++;
            ClearCells(position2);

            if (CheckConnectedCells(position2))
            {
                do
                {
                    multiplier++;
                    ClearCells(position2);
                } while(CheckConnectedCells(position2));
            }
            MoneyControl();

            ShowMerge(Field[position2.X, position2.Y]);
        }
        DisableSwap(true);
        SetAllCellsInteraction(true);

        //*/
    }

    public IEnumerator CheckMerge()
    {
        //        Debug.Log("CheckMerge");
        Cell startCell = Field[startPoint.X, startPoint.Y];
        Cell endCell = Field[endPoint.X, endPoint.Y];

        endCell.SetValue(startCell.Value);
        int randomColor;
        if (cellColors != null && cellColors.Length > 0)
        {
            randomColor = Random.Range(0, cellColors.Length);
            //                    go.GetComponent<SpriteRenderer>().material.color = cellColors[randomColor];
            endCell.gameObject.GetComponent<SpriteRenderer>().sprite = usedCellType[CurrentSkin];

            endCell.colorId = SetCorrespondingColor(endCell.gameObject.GetComponent<SpriteRenderer>(), startCell.Value);

        }
        startCell.ResetValue();
        //*
        if (Tutorial != 0)
        {
            if (!CheckConnectedCells(endPoint))
            {
                chainMerge = 0;
                //SetRandomCellsVal(cellsToSet, presetValues);
                StartCoroutine(SetRandomCellsVal());
                GeneratePresets(cellsToSet);

            }
            else
            {
                chainMerge++;
                multiplier++;
                ClearCells(endPoint);

                if (CheckConnectedCells(endPoint))
                {
                    do
                    {
                        yield return new WaitForSeconds(0.4f);
                        chainMerge++;
                        multiplier++;
                        ClearCells(endPoint);
                    } while(CheckConnectedCells(endPoint));
                }
                startPos = Field[endPoint.X, endPoint.Y].gameObject.transform.position;
                MoneyControl();
                if (chainMerge == 7)
                {
                    CallAddMoney(45);
                }
                else if (chainMerge == 5)
                {
                    CallAddMoney(15);
                }
                else if (chainMerge == 3)
                {
                    CallAddMoney(5);
                }
                if (enabledNow == PowerUps.None)
                    Field[endPoint.X, endPoint.Y].anim.Play("CellSpawn");
                ShowMerge(Field[endPoint.X, endPoint.Y]);

            }
        }
        else
        {
            if (CheckConnectedCells(endPoint))
            {

                ClearCells(endPoint);
            }
            TutorialStepManager();
        }

        inProgress = false;

    }

    /*/
    void ShakeCell()
    {
    }

    IEnumerator IShalke(GameObject toShake)
    {
        
    }

    //*/
    public void CallUnset()
    {
        try
        {
            Field[startPoint.X, startPoint.Y].UnsetPressed();
        }
        catch
        {
            Debug.Log("Oops!");
        }
    }

    void IncorrectPath()
    {
        if (Tutorial == 0)
            TutorialWrongPath();
        IncorrectPathPopUp(true);
        CallHideWrongPath(2f);

    }

    void CallHideWrongPath(float seconds)
    {
        StartCoroutine(HideWrongPath(seconds));
    }

    IEnumerator HideWrongPath(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        IncorrectPathPopUp(false);
    }

    public void IncorrectPathPopUp(bool show)
    {
        //        Debug.Log(show);
        incorrectMove.SetActive(show);
    }

    public void TryToSetCell()
    {
        //        Debug.Log("TTSC");
        inProgress = true;
        /*/
        if (Field[startPoint.X, startPoint.Y].Value == 0)
        {
            endPointMode = false;
            Field[startPoint.X, startPoint.Y].ResetValue();
            inProgress = false;
            return;
        }
        //*/
        List<Point> path = new List<Point>(FindPath(startPoint, endPoint));
        Field[startPoint.X, startPoint.Y].UnsetPressed();
        if (path.Count == 0)
        {
            IncorrectPath();
        }
        else
        {
            //TODO: animation
            StartCoroutine(MoveCell(path, Field[startPoint.X, startPoint.Y].gameObject));
            //*/
        }

    }

    void ClearCells(Point endPoint)
    {
        //Debug.Log("cellsToClear " + cellsToClear.Count);
        if (cellsToClear.Contains(endPoint))
            cellsToClear.Remove(endPoint);
        foreach (Point p in cellsToClear)
        {
            Field[p.X, p.Y].ResetValue();
        }
        int valToSet = Field[endPoint.X, endPoint.Y].Value * 4;
        if (valToSet < 8192)
        {
            Field[endPoint.X, endPoint.Y].SetValue(valToSet);
            Field[endPoint.X, endPoint.Y].colorId = SetCorrespondingColor(Field[endPoint.X, endPoint.Y].gameObject.GetComponent<SpriteRenderer>(), valToSet);
            AddScore(valToSet / 4 * (cellsToClear.Count + 1));
        }
        else
        {
            Field[endPoint.X, endPoint.Y].ResetValue();
            AddScore(valToSet / 4 * (cellsToClear.Count + 1));
        }
        if (valToSet >= 64)
        {
            totalMoney += 5;
        }

        UpdateScore();
        if (GetUsedCells().Count - cellToIgnore < 1)
        {
            GeneratePresets(cellsToSet);
            StartCoroutine(SetRandomCellsVal());
        }
    }

    void MoneyControl()
    {
        //Debug.Log("totalMoney " + totalMoney);
        //Debug.Log("multiplier " + multiplier);



        CallAddMoney(totalMoney * multiplier);

        totalMoney = 0;
        multiplier = 0;

    }
    //returns bool canMerge and tryes to do it if true
    bool CheckConnectedCells(Point start)
    {
        if (!Field[start.X, start.Y].used)
            return false;
        cellsToClear = new List<Point>();

        bool canMerge = false;
        List<Point> toMerge = new List<Point>();
        List<Point> newCells = new List<Point>(ToMerge(start, toMerge));
        List<Point> closed = new List<Point>();
        do
        {
            closed = MergeLists(closed, toMerge);
            toMerge = MergeLists(toMerge, newCells);
            newCells = new List<Point>();
            List<Point> tempList = new List<Point>();
            foreach (Point p in toMerge)
            {
                if (!closed.Contains(p))
                {
                    tempList = ToMerge(p, toMerge);
                    newCells = MergeLists(tempList, newCells);
                }

            }
        } while (closed.Count != toMerge.Count);
        if (toMerge.Count >= minToclear)
        {
            totalMoney += (toMerge.Count - minToclear + 1);
            canMerge = true;
            cellsToClear = new List<Point>(toMerge);
        }


        return canMerge;
    }

    List<T> MergeLists<T>(List<T> T1, List<T> T2)
    {
        List<T> tempList = new List<T>(T1);
        foreach (T item in T2)
        {
            if (!tempList.Contains(item))
                tempList.Add(item);
        }
        return tempList;
    }

    public List<Point>ToMerge(Point start, List<Point> preparedList)
    {
        int startValue = Field[start.X, start.Y].Value;
        List<Point> toCheck = new List<Point>();
        List<Point> finished = new List<Point>();
        toCheck = Field[start.X, start.Y].neighbours;
        foreach (Point p in toCheck)
        {
            if (Field[p.X, p.Y].Value == startValue && !preparedList.Contains(p))
            {
                finished.Add(p);
            }
        }

        return finished;
    }

    bool SearchById(List<NODE> checkList, Point searchVal)
    {
        foreach (NODE n in checkList)
        {
            if (n.id.X == searchVal.X && n.id.Y == searchVal.Y)
                return true;
        }
        return false;
    }


    public List<Point> FindPath(Point start, Point end)
    {
        //        Debug.Log("Start " + start.X + " " + start.Y);
        //        Debug.Log("end " + end.X + " " + end.Y);

        List<NODE> nodesHolder = new List<NODE>();
        List<Point> path = new List<Point>();
        Queue<NODE> queue = new Queue<NODE>();

        bool finished = false;
        NODE nStart;
        nStart.id = start;
        nStart.parent = new Point(-1, -1);
        nStart.level = 0;
        //        nStart.used = true;


        nodesHolder.Add(nStart);

        queue.Enqueue(nStart);
        //        Debug.Log("While start");

        while (queue.Count > 0 && !finished)
        {

            //Drops somewhere here
            NODE curNode = queue.Dequeue();
            int vertCount = Field[curNode.id.X, curNode.id.Y].neighbours.Count;
            for (int i = 0; i < vertCount; i++)
            {
                //                Debug.Log("<color=red>queue.Count </color>" + queue.Count);
                Point id = Field[curNode.id.X, curNode.id.Y].neighbours[i];
                if (!Field[id.X, id.Y].used)
                {
                    NODE tNode; 
                    tNode.id = id;
                    //                    tNode.used = true;
                    tNode.parent = curNode.id;
                    tNode.level = curNode.level + 1;
                    if (!SearchById(nodesHolder, tNode.id))
                    {
                        nodesHolder.Add(tNode);
                        queue.Enqueue(tNode);
                        if (tNode.id.Equals(end))
                        {
                            finished = true;
                        }
                    }
                }

            }
        }

        //*/


        //        Debug.Log("While end");
        Point index = end;

        for (int k = 0; k < nodesHolder.Count; k++)
        {
            NODE toSave = FindNode(nodesHolder, index);
            if (toSave.level == -1)
            {
                inProgress = false;
                //                Debug.Log("Stopping search, reseting Path - incorrect node type");
                path = new List<Point>();
                break;
            }
            path.Add(toSave.id);
            index = toSave.parent;
            if (index.Equals(nStart.parent))
                break;
        }


        endPointMode = false;
        path.Reverse();
        return path;

    }

    NODE FindNode(List<NODE> nodeList, Point cellId)
    {
        foreach (NODE n in nodeList)
        {

            if (n.id.Equals(cellId))
                return n;
        }
        //        Debug.Log("Can't find cell");
        NODE exception = new NODE();
        exception.level = -1;
        return exception;
    }

    public void SetRandomCellsVal(int cellsCount)
    {
        List<Cell> freeCells = new List<Cell>();
        for (int i = 0; i < fieldSize.X; i++)
        {
            for (int j = 0; j < fieldSize.Y; j++)
            {
                if (!Field[i, j].used)
                    freeCells.Add(Field[i, j]);
            }
        }

        if (cellsCount > freeCells.Count)
            cellsCount = freeCells.Count;

        //Debug.Log("CellsCount " + freeCells.Count);
        for (int i = 0; i < cellsCount; i++)
        {
            int randomIndex = Random.Range(0, freeCells.Count);
            int valToset = (int)Mathf.Pow(2, Random.Range(0, 4));
            freeCells[randomIndex].SetValue(valToset);
            int randomColor;
            if (cellColors != null && cellColors.Length > 0)
            {
                randomColor = Random.Range(0, cellColors.Length);
                //                    go.GetComponent<SpriteRenderer>().material.color = cellColors[randomColor];
                freeCells[randomIndex].gameObject.GetComponent<SpriteRenderer>().sprite = usedCellType[CurrentSkin];
                freeCells[randomIndex].colorId = SetCorrespondingColor(freeCells[randomIndex].gameObject.GetComponent<SpriteRenderer>(), valToset);

            }
            if (CheckConnectedCells(freeCells[randomIndex].cellPos))
            {
                int toCreate = cellsToClear.Count;
                if (cellsToClear.Contains(freeCells[randomIndex].cellPos))
                    toCreate--;
                ClearCells(freeCells[randomIndex].cellPos);
                SetRandomCellsVal(toCreate);
                freeCells = new List<Cell>();
                for (int z = 0; z < fieldSize.X; z++)
                {
                    for (int x = 0; x < fieldSize.Y; x++)
                    {
                        if (!Field[z, x].used)
                            freeCells.Add(Field[z, x]);
                    }
                }
                resetScore = true;
            }
            else
            {

                freeCells.RemoveAt(randomIndex);
            }
        }
        for (int i = 0; i < fieldSize.X; i++)
        {
            for (int j = 0; j < fieldSize.Y; j++)
            {
                //                Field[i, j].RestoreScale();
                Field[i, j].UnsetPressed();
            }
        }
        if (freeCells.Count == 0)
        {
            UIShowGameOver();
            return;
        }
        currentScore = 0;
        UpdateScore();
    }


    void GeneratePresets(int cellsCount)
    {
        presetValues = new int[cellsCount];
        //        presetValues = new int[]{ 1024, 1024, 1024, 1024 };
        //*/
        for (int i = 0; i < cellsCount; i++)
        {
            presetValues[i] = (int)Mathf.Pow(2, Random.Range(0, 4));
            //            spawnVal[i].text = presetValues[i].ToString();
            //            int randomColor = Random.Range(0, cellColors.Length);
            //            spawnVal[i].transform.parent.gameObject.GetComponent<SpriteRenderer>().color = cellColors[randomColor];
            //            SetCorrespondingColor(spawnVal[i].transform.parent.gameObject.GetComponent<SpriteRenderer>(), presetValues[i]);
        }
        //*/
    }

    bool CheckNeighbours(Point start)
    {
        List<Point> neighbours = Field[start.X, start.Y].neighbours;
        List<Point> sv = new List<Point>();
        for (int i = 0; i < neighbours.Count; i++)
        {
            if (Field[start.X, start.Y].Value == Field[neighbours[i].X, neighbours[i].Y].Value)
            {
                sv.Add(neighbours[i]);
            }
            if (sv.Count >= 2)
                return true;
        }
        neighbours = new List<Point>();
        //        Debug.Log(sv.Count);
        for (int i = 0; i < sv.Count; i++)
        {
            if (Field[start.X, start.Y].Value == Field[sv[i].X, sv[i].Y].Value)
            {
                neighbours.AddRange(Field[sv[i].X, sv[i].Y].neighbours);
            }
        }
        for (int i = 0; i < neighbours.Count; i++)
        {
            if (Field[start.X, start.Y].Value == Field[neighbours[i].X, neighbours[i].Y].Value)
            {
                sv.Add(neighbours[i]);
            }
            if (sv.Count >= 2)
                return true;
        }
        return false;
    }

    public IEnumerator SetRandomCellsVal()
	//void SetRandomCellsVal(int cellsCount, int[] toSetValues)
    {
        //        Debug.Log("SetRandomCellsVal");
        int[] toSetValues = presetValues;
        int cellsCount = toSetValues.Length;
        isAnimating = true;
        List<Cell> freeCells = new List<Cell>();
        for (int i = 0; i < fieldSize.X; i++)
        {
            for (int j = 0; j < fieldSize.Y; j++)
            {
                if (!Field[i, j].used)
                    freeCells.Add(Field[i, j]);
            }
        }
        if (cellsCount > freeCells.Count)
            cellsCount = freeCells.Count;

        //Debug.Log("CellsCount " + freeCells.Count);
        for (int i = 0; i < cellsCount; i++)
        {
            int randomIndex = Random.Range(0, freeCells.Count);
            freeCells[randomIndex].SetValue(toSetValues[i]);
            int randomColor;
            if (cellColors != null && cellColors.Length > 0)
            {
                //                randomColor = Random.Range(0, cellColors.Length);
                //                    go.GetComponent<SpriteRenderer>().material.color = cellColors[randomColor];
                freeCells[randomIndex].gameObject.GetComponent<SpriteRenderer>().sprite = usedCellType[CurrentSkin];


                freeCells[randomIndex].colorId = SetCorrespondingColor(freeCells[randomIndex].gameObject.GetComponent<SpriteRenderer>(), toSetValues[i]);
            }

            bool cleared = false;
            if (CheckNeighbours(freeCells[randomIndex].cellPos) && PlayMode == PlayMode.Extreme)
            {
                freeCells[randomIndex].ResetValue();

                randomIndex = Random.Range(0, freeCells.Count);
                freeCells[randomIndex].SetValue(toSetValues[i]);
                if (cellColors != null && cellColors.Length > 0)
                {
                    //                    randomColor = Random.Range(0, cellColors.Length);
                    //                    go.GetComponent<SpriteRenderer>().material.color = cellColors[randomColor];
                    freeCells[randomIndex].gameObject.GetComponent<SpriteRenderer>().sprite = usedCellType[CurrentSkin];


                    freeCells[randomIndex].colorId = SetCorrespondingColor(freeCells[randomIndex].gameObject.GetComponent<SpriteRenderer>(), toSetValues[i]);
                }
            }
            if (CheckConnectedCells(freeCells[randomIndex].cellPos))
            {
                //*/
                if (PlayMode != PlayMode.Classic)
                {
                    freeCells[randomIndex].ResetValue();
                    int preVal = toSetValues[i];
                    int exit = 0;
                    do
                    {
                    
                        toSetValues[i] = (int)Mathf.Pow(2, Random.Range(0, 4));
                        exit++;
                    } while(toSetValues[i] == preVal && exit > 5);

                    freeCells[randomIndex].SetValue(toSetValues[i]);
                    if (cellColors != null && cellColors.Length > 0)
                    {
                        //                    randomColor = Random.Range(0, cellColors.Length);
                        //                    go.GetComponent<SpriteRenderer>().material.color = cellColors[randomColor];
                        freeCells[randomIndex].gameObject.GetComponent<SpriteRenderer>().sprite = usedCellType[CurrentSkin];


                        freeCells[randomIndex].colorId = SetCorrespondingColor(freeCells[randomIndex].gameObject.GetComponent<SpriteRenderer>(), toSetValues[i]);
                    }
                }
                //*/
                #region MergePrevent
                /*/
                freeCells[randomIndex].ResetValue();

                randomIndex = Random.Range(0, freeCells.Count);
                freeCells[randomIndex].SetValue(toSetValues[i]);
                if (cellColors != null && cellColors.Length > 0)
                {
//                    randomColor = Random.Range(0, cellColors.Length);
                    //                    go.GetComponent<SpriteRenderer>().material.color = cellColors[randomColor];
                    freeCells[randomIndex].gameObject.GetComponent<SpriteRenderer>().sprite = usedCellType[CurrentSkin];


                    freeCells[randomIndex].colorId = SetCorrespondingColor(freeCells[randomIndex].gameObject.GetComponent<SpriteRenderer>(), toSetValues[i]);
                }
                //*/
                #endregion
                //*/
                if (PlayMode == PlayMode.Classic)
                {
                    multiplier++;
                    freeCells[randomIndex].RestoreScale();
                    freeCells[randomIndex].anim.Play("DissapearAfterAppear");
                    foreach (Point p in cellsToClear)
                    {
                        Field[p.X, p.Y].RestoreScale();
                        Field[p.X, p.Y].anim.Play("DissapearAfterAppear");

                    }

                    yield return new WaitForSeconds(0.5f);


                    ClearCells(freeCells[randomIndex].cellPos);
                    cleared = true;
                }
                //*/
                #region AdditionalMergePrevent
                /*/
                if (CheckConnectedCells(freeCells[randomIndex].cellPos))
                {
                    freeCells[randomIndex].ResetValue();

                    randomIndex = Random.Range(0, freeCells.Count);
                    freeCells[randomIndex].SetValue(toSetValues[i]);
                    if (cellColors != null && cellColors.Length > 0)
                    {
                        //                    randomColor = Random.Range(0, cellColors.Length);
                        //                    go.GetComponent<SpriteRenderer>().material.color = cellColors[randomColor];
                        freeCells[randomIndex].gameObject.GetComponent<SpriteRenderer>().sprite = usedCellType[CurrentSkin];


                        freeCells[randomIndex].colorId = SetCorrespondingColor(freeCells[randomIndex].gameObject.GetComponent<SpriteRenderer>(), toSetValues[i]);
                    }
                }
                //*/
                #endregion
                if (CheckConnectedCells(freeCells[randomIndex].cellPos))
                {
                    //                    inProgress = true;

                    do
                    {
                        multiplier++;
                        freeCells[randomIndex].RestoreScale();
                        freeCells[randomIndex].anim.Play("DissapearAfterAppear");
                        foreach (Point p in cellsToClear)
                        {
                            Field[p.X, p.Y].RestoreScale();
                            Field[p.X, p.Y].anim.Play("DissapearAfterAppear");

                        }

                        yield return new WaitForSeconds(0.5f);


                        ClearCells(freeCells[randomIndex].cellPos);
                    } while(CheckConnectedCells(freeCells[randomIndex].cellPos));


                }
            }
            startPos = freeCells[randomIndex].gameObject.transform.position;

            freeCells = new List<Cell>();
            for (int z = 0; z < fieldSize.X; z++)
            {
                for (int j = 0; j < fieldSize.Y; j++)
                {
                    if (!Field[z, j].used)
                        freeCells.Add(Field[z, j]);
                }
            }
            //            yield return new WaitForSeconds(0.2f);
        }
        MoneyControl();
        if (freeCells.Count == 0)
        {
            if (revived || TotalGames == 0)
            {
                for (int i = 0; i < fieldSize.X; i++)
                {
                    for (int j = 0; j < fieldSize.Y; j++)
                    {
                        Field[i, j].RestoreScale();
                        Field[i, j].anim.Play("DissapearAfterAppear");
                    }
                }

                yield return new WaitForSeconds(0.8f);

            }
            else
            {

                yield return new WaitForSeconds(0.5f);

            }

            UIShowGameOver();
            //return;
        }
        else if (freeCells.Count <= cellsToSet)
        {
            CheckPossibleMoves();
        }


        isAnimating = false;
    }

    bool CheckPossibleMoves()
    {
        List<Cell> freeCells = new List<Cell>();
        for (int i = 0; i < fieldSize.X; i++)
        {
            for (int j = 0; j < fieldSize.Y; j++)
            {
                if (!Field[i, j].used)
                    freeCells.Add(Field[i, j]);
            }
        }
        bool hasMerge = true;
        if (freeCells.Count <= cellsToSet)
        {
            int counter;
            hasMerge = false;
            foreach (Cell c in freeCells)
            {
                if (hasMerge)
                    break;
                List<int> usedValues = new List<int>();
                List<Point> possibleCells = new List<Point>();
                foreach (Point p in c.neighbours)//should ignore p cell
                {
                    if (hasMerge)
                        break;
                    //                    if (!Field[p.X, p.Y].used)
                    //                        continue;
                    counter = 0;
                    int cellValue = Field[p.X, p.Y].Value;
                    if (usedValues.Contains(cellValue))
                        continue;
                    List<Point> closed = new List<Point>();
                    List<Point> toCheck = new List<Point>();
                    toCheck.Add(c.cellPos);

                    while (toCheck.Count > 0 && counter < minToclear && !hasMerge)
                    {

                        hasMerge = counter >= minToclear;




                        if (!closed.Contains(toCheck[0]) && !p.Equals(toCheck[0]))
                        {
                            Point current = toCheck[0];
                            if (Field[current.X, current.Y].Value == cellValue || c.cellPos.Equals(current))
                            {
                                counter++;
                                for (int i = 0; i < Field[current.X, current.Y].neighbours.Count; i++)
                                {
                                    toCheck.AddRange(Field[current.X, current.Y].neighbours);
                                }
                            }
                        }
                        closed.Add(toCheck[0]);
                        toCheck.RemoveAt(0);
                    }
                    //                    Debug.Log("Counter " + counter);
                    //                    Debug.Log("toCheck.Count " + toCheck.Count);
                    //                    Debug.Log("hasMerge " + hasMerge);
                    //                    Debug.Log("X " + p.X + " Y " + p.Y);
                }
            }
        }
        if (!hasMerge)
            EnablePowerAnim();
        return hasMerge;
    }

    public void ShowNeighbours(Point coreCell)
    {
        List<Point> neigh = Field[coreCell.X, coreCell.Y].neighbours;
        for (int i = 0; i < fieldSize.X; i++)
        {
            for (int k = 0; k < fieldSize.Y; k++)
            {

                //                Field[i, k].UnsetPressed();
                //                Field[i, k].RestoreScale();
                if (Field[i, k].Value != 9999)
                {
                    Field[i, k].gameObject.SetActive(false);

                    Field[i, k].gameObject.SetActive(true);
                }

            }
        }
        foreach (Point p in neigh)
        {
            //            Field[p.X, p.Y].anim.Stop();

            Field[p.X, p.Y].anim.Play("CellPulse");
        }
    }

    int SetCorrespondingColor(SpriteRenderer rend, int value)
    {
        int colorIndex = (int)Mathf.Log(value, 2f);
        //        Debug.Log("log " + colorIndex);
        /*/
        Debug.Log("value " + value);
        Debug.Log("colorIndex" + colorIndex);
        //*/
        if (colorIndex >= BaseSet.Length)
            colorIndex = BaseSet.Length - 1;
        rend.color = cellColors[colorIndex];
        return colorIndex;
    }

}
